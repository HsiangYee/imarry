<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\ECPayController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\ConsolePageController;

Route::group(['prefix' => 'console'], function() {
    Route::get('signin', [ConsolePageController::class, 'signin'])->name('console.signin');
    Route::get('sugnup', [ConsolePageController::class, 'signup'])->name('console.signup');

    Route::group(['middleware' => 'AccountPageMiddleware'], function() {
        Route::get('/', [ConsolePageController::class, 'index'])->name('console.index');
        Route::get('profile', [ConsolePageController::class, 'profile'])->name('console.profile');

        Route::group(['prefix' => 'project'], function() {
            Route::get('/', [ConsolePageController::class, 'project'])->name('console.project');
            Route::get('info/{id}', [ConsolePageController::class, 'project_info'])->name('console.project_info');
        }); 
    });
});

Route::get('form/{id}', [PageController::class, 'form'])->name('page.form');
Route::get('formSuccess/{id}', [PageController::class, 'form_success'])->name('page.form.success');
Route::get('pay/{id}', [PageController::class, 'pay'])->name('page.pay');
Route::post('ecpay', [ECPayController::class, 'sendOrder'])->name('page.ecpay');

Route::group(['prefix' => 'ecpay'], function() {
    Route::post('return', [ECPayController::class, 'returnUrl'])->name('ecpay.return');
});
