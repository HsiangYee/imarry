<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AccountController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\GuestTypeController;
use App\Http\Controllers\GuestAttendController;
use App\Http\Controllers\FormDescriptionController;
use App\Http\Controllers\CashGiftController;

Route::group(['prefix' => 'account'], function() {
    Route::post('signin', [AccountController::class, 'signin'])->name('api.account.signin');
    Route::post('signup', [AccountController::class, 'signup'])->name('api.account.signup');
    Route::post('emailVerification', [AccountController::class, 'emailVerification'])->name('api.account.emailVerification');

    Route::group(['middleware' => 'AccountAPIMiddleware'], function() {
        Route::put('update', [AccountController::class, 'update'])->name('api.account.update');
        Route::post('signout', [AccountController::class, 'signout'])->name('api.account.signout');
        Route::post('accountInfo', [AccountController::class, 'accountInfo'])->name('api.account.accountInfo');
        Route::post('otherEmailVerification', [AccountController::class, 'otherEmailVerification'])->name('api.account.otherEmailVerification');

        Route::group(['prefix' => 'project'], function() {
            Route::post('create', [ProjectController::class, 'create'])->name('api.account.project.create');
            Route::put('update', [ProjectController::class, 'update'])->name('api.account.project.update');
            Route::delete('delete', [ProjectController::class, 'delete'])->name('api.account.project.delete');
            Route::post('search', [ProjectController::class, 'search'])->name('api.account.project.search');
            Route::post('searchByID', [ProjectController::class, 'searchByID'])->name('api.account.project.searchByID');
        });

        Route::group(['prefix' => 'form'], function() {
            Route::group(['prefix' => 'description'], function() {
                Route::put('update', [FormDescriptionController::class, 'update'])->name('api.account.form.description.update');
                Route::post('searchByProjectID', [FormDescriptionController::class, 'searchByProjectID'])->name('api.account.form.description.searchByProjectID');
            });

            Route::group(['prefix' => 'guestType'], function() {
                Route::post('create', [GuestTypeController::class, 'create'])->name('api.account.form.guestType.create');
                Route::put('update', [GuestTypeController::class, 'update'])->name('api.account.form.guestType.update');
                Route::delete('delete', [GuestTypeController::class, 'delete'])->name('api.account.form.guestType.delete');
                Route::post('searchByProjectID', [GuestTypeController::class, 'searchByProjectID'])->name('api.account.form.guestType.searchByProjectID');
            });

            Route::group(['prefix' => 'guestAttend'], function() {
                Route::delete('delete', [GuestAttendController::class, 'delete'])->name('api.account.form.guestAttend.delete');
                Route::post('searchByProjectID', [GuestAttendController::class, 'searchByProjectID'])->name('api.account.form.guestAttend.searchByProjectID');
            });
        });

        Route::group(['prefix' => 'cashGift'], function() {
            Route::post('create', [CashGiftController::class, 'create'])->name('api.account.form.cashGift.create');
            Route::put('update', [CashGiftController::class, 'update'])->name('api.account.form.cashGift.update');
            Route::delete('delete', [CashGiftController::class, 'delete'])->name('api.account.form.cashGift.delete');
            Route::post('searchByProjectID', [CashGiftController::class, 'searchByProjectID'])->name('api.account.form.cashGift.searchByProjectID');
        });
    });
});

Route::group(['prefix' => 'form'], function() {
    Route::post('create', [GuestAttendController::class, 'create'])->name('form.create');
});