<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGuestAttendTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guest_attend', function (Blueprint $table) {
            $table->id();
            $table->integer('project_ID')->comment('專案ID');
            $table->integer('guest_type_ID')->comment('賓客類別');
            $table->string('name')->comment('賓客姓名');
            $table->string('attend')->comment('是否出席');
            $table->integer('people')->comment('出席人數');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guests_attend');
    }
}
