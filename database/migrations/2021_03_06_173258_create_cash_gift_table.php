<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCashGiftTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cash_gift', function (Blueprint $table) {
            $table->id();
            $table->integer('project_ID')->comment('專案 ID');
            $table->string('name')->comment('姓名');
            $table->integer('amount')->comment('金額');
            $table->string('orderCode')->nullable()->comment('交易序號');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cash_gift');
    }
}
