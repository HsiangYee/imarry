<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf8">
        <title>iMarry | 出席表單</title>

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ URL('resources/template/plugins/fontawesome-free/css/all.min.css') }}">

        <!-- Theme style -->
        <link rel="stylesheet" href="{{ URL('resources/template/dist/css/adminlte.min.css') }}">
        <!-- SweetAlert2 -->
        <link rel="stylesheet" href="{{ URL('resources/template/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
        <!-- Toastr -->
        <link rel="stylesheet" href="{{ URL('resources/template/plugins/toastr/toastr.min.css') }}">
        <!-- jQuery -->
        <script src="{{ URL('resources/template/plugins/jquery/jquery.min.js') }}"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="{{ URL('resources/template/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
        <!-- juqery-conform -->
        <link rel="stylesheet" href="{{ URL('resources/static/libraries/jquery-confirm/jquery-confirm.min.css') }}">
        <script src="{{ URL('resources/static/libraries/jquery-confirm/jquery-confirm.min.js') }}"></script>
        <!-- Custom -->
        <link rel="stylesheet" href="{{ URL('resources/static/css/custom.css') }}">
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                i<b>Marry</b>
            </div>
            <div class="card">
                <div class="card-body login-card-body">
                    <div class="mb-2">
                        {{ $description }}
                    </div>

                    <div class="form-group mb-3">
                        <label>姓名<span class="text-danger">*</span></label>
                        <input type="text" id="name" class="form-control" placeholder="姓名" autocomplete="off">
                        <div class="invalid-feedback" id="name-invalid-message"></div>
                    </div>

                    <div class="form-group mb-3">
                        <label>身份別<span class="text-danger">*</span></label>
                        <select class="form-control border" id="guestType">
                            @foreach ($guestType as $type)
                                <option value="{{ $type['id'] }}">{{ $type['description'] }}</option>
                            @endforeach
                        </select>
                        
                        <div class="invalid-feedback" id="guestType-invalid-message"></div>
                    </div>

                    <div class="form-group mb-3">
                        <label>是否出席<span class="text-danger">*</span></label>
                        <select class="form-control border" id="attend">
                            <option value="yes">我會出席</option>
                            <option value="no">我無法出席</option>
                        </select>
                        
                        <div class="invalid-feedback" id="attend-invalid-message"></div>
                    </div>

                    <div class="form-group mb-3">
                        <label>出席人數<span class="text-danger">*</span></label>
                        <input type="number" id="number" min=0 class="form-control" placeholder="出席人數" autocomplete="off" value="1">
                        <div class="invalid-feedback" id="number-invalid-message"></div>
                    </div>

                    <button class="btn btn-success btn-sm btn-block" onclick="submitForm()" id="loginBtn">送 出 表 單</button>
                </div>
            </div>
        </div>
    </body>
    <!-- Bootstrap 4 -->
    <script src="{{ URL('resources/template/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- Sparkline -->
    <script src="{{ URL('resources/template/plugins/sparklines/sparkline.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ URL('resources/template/dist/js/adminlte.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ URL('resources/template/dist/js/demo.js') }}"></script>
    <!-- SweetAlert2 -->
    <script src="{{ URL('resources/template/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <!-- Toastr -->
    <script src="{{ URL('resources/template/plugins/toastr/toastr.min.js') }}"></script>
    <!-- form -->
    <script src="{{ URL('resources/static/js/form.js') }}"></script>
    <!-- alert -->
    <script src="{{ URL('resources/static/js/alert.js') }}"></script>

    <script>
        const project_ID = "{{ $project_ID }}";
        const submitFormAPI = "{{ route('form.create') }}";
        const formSuccessRoute = "{{ route('page.form.success', ['id' => $project_ID]) }}";
    </script>
    <script>
        function submitForm() {
            const name = $('#name');
            const guestType = $('#guestType');
            const attend = $('#attend');
            const number = $('#number');

            let input_hasNull = false;
            input_hasNull = input_hasNull || input_isNull(name);
            input_hasNull = input_hasNull || input_isNull(guestType);
            input_hasNull = input_hasNull || input_isNull(attend);
            input_hasNull = input_hasNull || input_isNull(number);

            input_is_invalid(name);
            input_is_invalid(guestType);
            input_is_invalid(attend);
            input_is_invalid(number);

            if (!input_hasNull) {
                let data = {
                    'project_ID': project_ID,
                    'name': name.val(),
                    'guest_type_ID': guestType.val(),
                    'attend': attend.val(),
                    'people': number.val()
                }
                
                submitFormRequest(data);
            }
        }
        
        function submitFormRequest(data) {
            $.ajax({
                url: submitFormAPI,
                type: 'post',
                data: data,
            }).done(function(response) {
                if (response['status'] == 'success') {
                    location.assign(formSuccessRoute);
                } else if (response['status'] == 'error') {
                    toast.fire('error', response['message']);
                } else {
                    toast.fire('warning', '無法辨識伺服器回傳資料');
                }
            }).fail(function() {
                toast.fire('warning', '無法向伺服器傳送資料');
            });
        }
    </script>
</html>