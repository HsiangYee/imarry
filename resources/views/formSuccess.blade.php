<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf8">
        <title>iMarry | 交易明細</title>

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ URL('resources/template/plugins/fontawesome-free/css/all.min.css') }}">

        <!-- Theme style -->
        <link rel="stylesheet" href="{{ URL('resources/template/dist/css/adminlte.min.css') }}">
        <!-- SweetAlert2 -->
        <link rel="stylesheet" href="{{ URL('resources/template/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
        <!-- Toastr -->
        <link rel="stylesheet" href="{{ URL('resources/template/plugins/toastr/toastr.min.css') }}">
        <!-- jQuery -->
        <script src="{{ URL('resources/template/plugins/jquery/jquery.min.js') }}"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="{{ URL('resources/template/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
        <!-- juqery-conform -->
        <link rel="stylesheet" href="{{ URL('resources/static/libraries/jquery-confirm/jquery-confirm.min.css') }}">
        <script src="{{ URL('resources/static/libraries/jquery-confirm/jquery-confirm.min.js') }}"></script>
        <!-- Custom -->
        <link rel="stylesheet" href="{{ URL('resources/static/css/custom.css') }}">
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                i<b>Marry</b>
            </div>
            <div class="card">
                <div class="card-body login-card-body">
                    <div class="mb-2 text-center">
                        已收到您的表單回覆
                    </div>

                    <a href="{{ route('page.pay', ['id' => $project_ID]) }}" class="btn btn-success btn-sm btn-block">線上支付禮金</a>
                </div>
            </div>
        </div>
    </body>
    <!-- Bootstrap 4 -->
    <script src="{{ URL('resources/template/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- Sparkline -->
    <script src="{{ URL('resources/template/plugins/sparklines/sparkline.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ URL('resources/template/dist/js/adminlte.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ URL('resources/template/dist/js/demo.js') }}"></script>
    <!-- SweetAlert2 -->
    <script src="{{ URL('resources/template/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <!-- Toastr -->
    <script src="{{ URL('resources/template/plugins/toastr/toastr.min.js') }}"></script>
    <!-- form -->
    <script src="{{ URL('resources/static/js/form.js') }}"></script>
    <!-- alert -->
    <script src="{{ URL('resources/static/js/alert.js') }}"></script>
</html>