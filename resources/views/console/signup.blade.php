<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf8">
        <title>iMarry | 註冊</title>

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ URL('resources/template/plugins/fontawesome-free/css/all.min.css') }}">

        <!-- Theme style -->
        <link rel="stylesheet" href="{{ URL('resources/template/dist/css/adminlte.min.css') }}">
        <!-- SweetAlert2 -->
        <link rel="stylesheet" href="{{ URL('resources/template/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
        <!-- Toastr -->
        <link rel="stylesheet" href="{{ URL('resources/template/plugins/toastr/toastr.min.css') }}">
        <!-- jQuery -->
        <script src="{{ URL('resources/template/plugins/jquery/jquery.min.js') }}"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="{{ URL('resources/template/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
        <!-- juqery-conform -->
        <link rel="stylesheet" href="{{ URL('resources/static/libraries/jquery-confirm/jquery-confirm.min.css') }}">
        <script src="{{ URL('resources/static/libraries/jquery-confirm/jquery-confirm.min.js') }}"></script>
        <!-- Custom -->
        <link rel="stylesheet" href="{{ URL('resources/static/css/custom.css') }}">
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                i<b>Marry</b>
            </div>
            <div class="card">
                <div class="card-body login-card-body">
                    <div class="input-group mb-3">
                        <input type="text" id="name" class="form-control" placeholder="姓名" autocomplete="off">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-user"></span>
                            </div>
                        </div>
                        <div class="invalid-feedback" id="name-invalid-message"></div>
                    </div>

                    <div class="input-group mb-3">
                        <input type="text" id="email" class="form-control" placeholder="信箱" autocomplete="off">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                        <div class="invalid-feedback" id="email-invalid-message"></div>
                    </div>

                    <div class="input-group mb-3">
                        <input type="password" id="password" class="form-control" placeholder="密碼">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                        <div class="invalid-feedback" id="password-invalid-message"></div>
                    </div>

                    <div class="input-group mb-3">
                        <input type="password" id="checkPassword" class="form-control" placeholder="確認密碼">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                        <div class="invalid-feedback" id="checkPassword-invalid-message"></div>
                    </div>
                    <button class="btn btn-success btn-sm btn-block" onclick="signup()" id="loginBtn">註 冊</button>
                    <div class="text-center my-2"> or </div>
                    <a href="{{ route('console.signin') }}" class="btn btn-info btn-sm btn-block text-white" id="loginBtn">已 有 帳 號 ， 前 往 登 入</a>
                </div>
            </div>
        </div>
    </body>
    <!-- Bootstrap 4 -->
    <script src="{{ URL('resources/template/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- Sparkline -->
    <script src="{{ URL('resources/template/plugins/sparklines/sparkline.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ URL('resources/template/dist/js/adminlte.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ URL('resources/template/dist/js/demo.js') }}"></script>
    <!-- SweetAlert2 -->
    <script src="{{ URL('resources/template/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <!-- Toastr -->
    <script src="{{ URL('resources/template/plugins/toastr/toastr.min.js') }}"></script>
    <!-- form -->
    <script src="{{ URL('resources/static/js/form.js') }}"></script>
    <!-- alert -->
    <script src="{{ URL('resources/static/js/alert.js') }}"></script>

    <script>
        const signupAPI = "{{ route('api.account.signup') }}";
        const emailVerificationAPI = "{{ route('api.account.emailVerification') }}";
        const accountSigninRoute = "{{ route('console.signin') }}";
    </script>
    <script>
        async function emailVerification() {
            $.ajax({
                url: emailVerificationAPI,
                type: 'post',
                data: {
                    'email' : $('#email').val()
                }
            }).done(function(response) {
                if (response['status'] == 'success') {
                    if ($('#email').hasClass('is-invalid')) {
                        $('#email').removeClass('is-invalid');
                    }

                    return true;
                } else if (response['status'] == 'error') {
                    if (!$('#email').hasClass('is-invalid')) {
                        $('#email').toggleClass('is-invalid');
                        $(`#email-invalid-message`).html('不可使用此信箱');
                    }

                    return false;
                } else {
                    toast.fire('warning', '無法辨識伺服器回傳資料');

                    return false;
                }
            }).fail(function() {
                toast.fire('warning', '無法向伺服器傳送資料')
                return false;
            });
        }

        async function signup() {
            const name = $('#name');
            const email = $('#email');
            const password = $('#password');
            const checkPassword = $('#checkPassword');

            let input_hasNull = false;
            input_hasNull = input_hasNull || input_isNull(name);
            input_hasNull = input_hasNull || input_isNull(email);
            input_hasNull = input_hasNull || input_isNull(password);
            input_hasNull = input_hasNull || input_isNull(checkPassword);

            input_is_invalid(name);
            input_is_invalid(email);
            input_is_invalid(password);
            input_is_invalid(checkPassword);
            

            let error = 0;
            $('#loginBtn').toggleClass('disabled');
            if (input_hasNull) {
                error ++;
            }

            if (!input_check(password, checkPassword)) {
                error ++;
            }

            if (await !emailVerification()){
                error ++;
            }

            if ($('input.is-invalid').length > 0) {
                error ++;
            }

            if (error == 0) {
                let data = {
                    'name' : name.val(),
                    'email' : email.val(),
                    'password' : password.val(),
                    'checkPassword' : checkPassword.val(),
                };

                signupRequest(data);
            } else {
                $('#loginBtn').toggleClass('disabled');
            };
        }

        function signupRequest(data) {
            $.ajax({
                url: signupAPI,
                type: 'post',
                data: data
            }).done(function(response) {
                $('#loginBtn').removeClass('disabled');
                if (response['status'] == 'success') {
                    toast.fire('success', '註冊成功，三秒後跳轉登入頁面');
                    setTimeout(() => {
                        location.assign(accountSigninRoute);    
                    }, 3000);
                } else if (response['status'] == 'error') {
                    toast.fire('error', response['message']);
                } else {
                    toast.fire('warning', '無法辨識伺服器回傳資料');
                }
            }).fail(function() {
                toast.fire('warning', '無法向伺服器傳送資料');
            });
        }
    </script>
</html>