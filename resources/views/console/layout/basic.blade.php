<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf8">
        <title>iMarry</title>

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ URL('resources/template/plugins/fontawesome-free/css/all.min.css') }}">
        <!-- Ionicons -->
        <!-- <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> -->
        <!-- Tempusdominus Bbootstrap 4 -->
        <!-- <link rel="stylesheet" href="{{ URL('resources/template/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}"> -->
        <!-- iCheck -->
        <!-- <link rel="stylesheet" href="{{ URL('resources/template/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}"> -->
        <!-- JQVMap -->
        <!-- <link rel="stylesheet" href="{{ URL('resources/template/plugins/jqvmap/jqvmap.min.css') }}"> -->
        <!-- overlayScrollbars -->
        <!-- <link rel="stylesheet" href="{{ URL('resources/template/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}"> -->
        <!-- Daterange picker -->
        <!-- <link rel="stylesheet" href="{{ URL('resources/template/plugins/daterangepicker/daterangepicker.css') }}"> -->
        <!-- summernote -->
        <!-- <link rel="stylesheet" href="{{ URL('resources/template/plugins/summernote/summernote-bs4.css') }}"> -->

        <!-- Theme style -->
        <link rel="stylesheet" href="{{ URL('resources/template/dist/css/adminlte.min.css') }}">
        <!-- SweetAlert2 -->
        <link rel="stylesheet" href="{{ URL('resources/template/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
        <!-- Toastr -->
        <link rel="stylesheet" href="{{ URL('resources/template/plugins/toastr/toastr.min.css') }}">
        <!-- jQuery -->
        <script src="{{ URL('resources/template/plugins/jquery/jquery.min.js') }}"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="{{ URL('resources/template/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
        <!-- juqery-conform -->
        <link rel="stylesheet" href="{{ URL('resources/static/libraries/jquery-confirm/jquery-confirm.min.css') }}">
        <script src="{{ URL('resources/static/libraries/jquery-confirm/jquery-confirm.min.js') }}"></script>
        <!-- Custom -->
        <link rel="stylesheet" href="{{ URL('resources/static/css/custom.css') }}">

        @yield('head')
    </head>
    <body class="hold-transition sidebar-mini layout-fixed">
        <div class="wrapper">
            <nav class="main-header navbar navbar-expand navbar-white navbar-light">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                    </li>
                </ul>
            </nav>

            <aside class="main-sidebar sidebar-dark-primary elevation-4">
                <div class="brand-link text-center">
                    <span class="brand-text font-weight-light">i<B>Marry</B> 後臺管理</span>
                </div>
                <div class="sidebar">
                    <nav class="mt-2">
                        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                            <li class="nav-item">
                                <a href="{{ route('console.project') }}" class="nav-link">
                                    <i class="nav-icon fas fa-folder"></i>
                                    <p>專案列表</p>
                                </a>
                            </li>

                            <li class="nav-header">使用者</li>
                            <li class="nav-item">
                                <a href="{{ route('console.profile') }}" class="nav-link">
                                    <i class="nav-icon fas fa-address-card"></i>
                                    <p>個人資料</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link" onclick="signout()">
                                    <i class="nav-icon fas fa-cogs"></i>
                                    <p>登出</p>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </aside>

            <div class="content-wrapper">
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h1 class="m-0 text-dark">@yield('title')</h1>
                            </div>
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    @yield('breadcrumb') 
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>

                <section class="content">
                    @yield('content')
                </section>
            </div>
            
            <footer class="main-footer">
                <strong>2021 iMarry</strong>
                <div class="float-right d-none d-sm-inline-block">
                    <b>Power by <a href="http://adminlte.io">AdminLTE.io</a></b> 
                </div>
            </footer>
        </div>
    </body>

    <!-- JQVMap -->
    <!-- <script src="{{ URL('resources/template/plugins/jqvmap/jquery.vmap.min.js') }}"></script> -->
    <!-- <script src="{{ URL('resources/template/plugins/jqvmap/maps/jquery.vmap.usa.js') }}"></script> -->
    <!-- jQuery Knob Chart -->
    <!-- <script src="{{ URL('resources/template/plugins/jquery-knob/jquery.knob.min.js') }}"></script> -->
    <!-- daterangepicker -->
    <!-- <script src="{{ URL('resources/template/plugins/moment/moment.min.js') }}"></script> -->
    <!-- <script src="{{ URL('resources/template/plugins/daterangepicker/daterangepicker.js') }}"></script> -->
    <!-- Tempusdominus Bootstrap 4 -->
    <!-- <script src="{{ URL('resources/template/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script> -->
    <!-- Summernote -->
    <!-- <script src="{{ URL('resources/template/plugins/summernote/summernote-bs4.min.js') }}"></script> -->
    <!-- overlayScrollbars -->
    <!-- <script src="{{ URL('resources/template/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script> -->
    <!-- ChartJS -->
    <!-- <script src="{{ URL('resources/template/plugins/chart.js/Chart.min.js') }}"></script> -->
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <!-- <script src="{{ URL('resources/template/dist/js/pages/dashboard.js') }}"></script> -->

    <!-- Bootstrap 4 -->
    <script src="{{ URL('resources/template/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- Sparkline -->
    <script src="{{ URL('resources/template/plugins/sparklines/sparkline.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ URL('resources/template/dist/js/adminlte.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ URL('resources/template/dist/js/demo.js') }}"></script>
    <!-- SweetAlert2 -->
    <script src="{{ URL('resources/template/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <!-- Toastr -->
    <script src="{{ URL('resources/template/plugins/toastr/toastr.min.js') }}"></script>
    <!-- form -->
    <script src="{{ URL('resources/static/js/form.js') }}"></script>
    <!-- alert -->
    <script src="{{ URL('resources/static/js/alert.js') }}"></script>

    <script>
        const signOutAPI = "{{ route('api.account.signout') }}";
        const signinRoute = "{{ route('console.signin') }}";
    </script>

    <script>
        function signout() {
            $.ajax({
                url: signOutAPI,
                type: 'post'
            }).done(function(response) {
                if (response['status'] == 'success') {
                    location.assign(signinRoute);
                } else if (response['status'] == 'error') {
                    toast.fire('error', response['message']);
                } else {
                    toast.fire('warning', '無法辨識伺服器回傳資料');
                }
            }).fail(function() {
                toast.fire('warning', '無法向伺服器傳送資料');
            });
        }
    </script>
    @yield('script')
</html>