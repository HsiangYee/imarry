@extends('console/layout/basic')

@section('title', '專案管理')

@section('breadcrumb')
    <li class="breadcrumb-item active">專案管理</li>
@endsection

@section('content')
<div class="card card-info card-outline">
    <div class="card-header py-2">
        <div class="card-tools">
            <button onclick="createProject()" class="btn btn-sm btn-info"><i class="fas fa-plus"></i>&nbsp;&nbsp;建 立 專 案</button>
        </div>
    </div>
    <div class="card-body table-responsive p-0">
        <table class="table table-hover custom-table-tr-pointer" id="projectList">
            <thead>
                <tr>
                    <th>專案名稱</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="text-center">取得資料中...</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('script')
<script>
    const searchProjectAPI = "{{ route('api.account.project.search') }}";
    const createProjectAPI = "{{ route('api.account.project.create') }}";
</script>
<script>
    function createProject() {
        let content = `
            <div class="form-group">
                <label>專案名稱<span class="text-danger">*</span></label>
                <input type="text" class="form-control" id="name" placeholder="輸入專案名稱" autocomplete="off">
                <div class="invalid-feedback" id="name-invalid-message"></div>
            </div>

            <div class="form-group">
                <label>描述<span class="text-danger">*</span></label>
                <textarea class="form-control" id="description" placeholder="輸入描述"></textarea>
                <div class="invalid-feedback" id="description-invalid-message"></div>
            </div>
        `;

        $.confirm({
            title: '建立專案',
            type: 'blue',
            content: content,
            buttons: {
                save: {
                    text: '建 立',
                    btnClass: 'btn btn-success',
                    action: function() {
                        const name = $('#name');
                        const description = $('#description');

                        let input_hasNull = false;
                        input_hasNull = input_hasNull || input_isNull(name);
                        input_hasNull = input_hasNull || input_isNull(description);

                        input_is_invalid(name);
                        input_is_invalid(description);

                        if (!input_hasNull) {
                            let data = {
                                'name': name.val(),
                                'description': description.val()
                            };
                            createRequest(data);
                        } else {
                            return false;
                        }
                    }
                },
                cancel: {
                    text: '取 消'
                }
            }
        });
    }

    function createRequest(data) {
        $.ajax({
            url: createProjectAPI,
            type: 'post',
            data: data,
        }).done(function(response) {
            if (response['status'] == 'success') {
                toast.fire('success', '建立成功');
                searchProjectRequest();
            } else if (response['status'] == 'error') {
                toast.fire('error', response['message']);
            } else {
                toast.fire('warning', '無法辨識伺服器回傳資料');
            }
        }).fail(function() {
            toast.fire('warning', '無法向伺服器傳送資料');
        });
    }

    function searchProjectRequest() {
        $.ajax({
            url: searchProjectAPI,
            type: 'post',
            data: {
                'name' : $('#name').val(),
            },
        }).done(function(response) {
            if (response['status'] == 'success') {
                putProject(response['message'])
            } else if (response['status'] == 'error') {
                toast.fire('error', response['message']);
            } else {
                toast.fire('warning', '無法辨識伺服器回傳資料');
            }
        }).fail(function() {
            toast.fire('warning', '無法向伺服器傳送資料');
        });
    }

    function putProject(data) {
        console.log(data);
        let content = '';
        data.forEach(function(value, index) {
            content += `
                <tr onclick="location.assign('${value['info_route']}')">
                    <td>${value['name']}</td>
                </tr>
            `;
        });

        $('#projectList tbody').html(content);
    }

    searchProjectRequest();
</script>
@endsection