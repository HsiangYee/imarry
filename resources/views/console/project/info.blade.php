@extends('console/layout/basic')

@section('title', '專案管理')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('console.project') }}">專案列表</a></li>
    <li class="breadcrumb-item active">專案管理</li>
@endsection

@section('content')
<div class="row">
    <div class="col">
        <div class="card card-info card-outline card-tabs">
            <div class="card-header p-0 pt-1 border-bottom-0">
                <!-- 上行系所資料列 -->
                <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="project-info-tab" data-toggle="pill" href="#project-info" aria-selected="true">專案資訊</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" id="guest-management-tab" data-toggle="pill" href="#guest-management" aria-selected="true">賓客管理</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" id="payment-flow-management-tab" data-toggle="pill" href="#payment-flow-management" aria-selected="true">禮金簿管理</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" id="project-management-tab" data-toggle="pill" href="#project-management" aria-selected="true">專案管理</a>
                    </li>
                </ul>
            </div>
            <div class="card-body p-0">
                <div class="tab-content" id="score-info-tabContent">
                    <div class="tab-pane fade active show" id="project-info" role="tabpanel" aria-labelledby="project-info-tab">
                        <div class="row p-2">
                            <div class="col-lg-3 col-6">
                                <div class="small-box bg-info">
                                    <div class="inner">
                                        <h3 id="people">0</h3>

                                        <p>出席人數</p>
                                    </div>
                                    <div class="icon">
                                        <i class="ion ion-bag"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-6">
                                <div class="small-box bg-success">
                                    <div class="inner">
                                        <h3 id="blanace"></h3>

                                        <p>已收帳款</p>
                                    </div>
                                    <div class="icon">
                                        <i class="ion ion-stats-bars"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-6"></div>
                            <div class="col-lg-3 col-6"></div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="guest-management" role="tabpanel" aria-labelledby="guest-management-tab">
                        <div class="row p-2">
                            <div class="col-5 col-sm-3">
                                <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">
                                    <a class="nav-link active" id="form-tab" data-toggle="pill" href="#form" role="tab" aria-controls="form" aria-selected="true">表單管理</a>
                                    <a class="nav-link" id="reply-tab" data-toggle="pill" href="#reply" role="tab" aria-controls="reply" aria-selected="true">回覆列表 (<span id="guestAttendNumber"></span>)</a>
                                </div>
                            </div>
                            <div class="col-7 col-sm-9">
                                <div class="tab-content" id="vert-tabs-tabContent">
                                    <div class="tab-pane text-left fade show active" id="form" label="tab" role="tabpanel" aria-labelledby="form-tab">
                                        <div class="text-right mb-2">
                                            <button onclick="createProject()" class="btn btn-sm btn-info"><i class="fas fa-save"></i>&nbsp;&nbsp;儲 存</button>
                                            <a href="{{ route('page.form', ['id' => $project_ID]) }}" target="_blank" class="btn btn-sm btn-info"><i class="far fa-eye"></i>&nbsp;&nbsp;檢 視 表 單</a>
                                        </div>

                                        <div class="h4 text-info"><B>表單描述</B></div>
                                        <div class="form-group mb-3">
                                            <textarea id="form-description" class="form-control"></textarea>
                                            <div class="invalid-feedback" id="form-description-invalid-message"></div>
                                        </div>

                                        <div class="h4 text-info"><B>賓客類別</B></div>
                                        <table class="table table-hover" id="guestTypeList">
                                            <thead>
                                                <tr>
                                                    <th width="80%">類別名稱</th>
                                                    <th width="20%" class="text-center">操作</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="text-center" colspan="2">取得資料中...</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="tab-pane text-left fade" id="reply" label="tab" role="tabpanel" aria-labelledby="reply-tab">
                                        <div class="text-right mb-2">
                                            <button onclick="getGuestAttendRequest()" class="btn btn-sm btn-info"><i class="fas fa-redo"></i>&nbsp;&nbsp;重 新 整 理</button>
                                        </div>

                                        <div class="h4 text-info"><B>回覆列表</B></div>
                                        <table class="table table-hover" id="guestAttendList">
                                            <thead>
                                                <tr>
                                                    <th width="10%" class="text-center">#</th>
                                                    <th width="20%" class="text-center">姓名</th>
                                                    <th width="20%" class="text-center">身份別</th>
                                                    <th width="20%" class="text-center">是否出席</th>
                                                    <th width="10%" class="text-center">人數</th>
                                                    <th width="20%" class="text-center" class="text-center">操作</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="text-center" colspan="6">取得資料中...</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="payment-flow-management" role="tabpanel" aria-labelledby="payment-flow-management-tab">
                        <div class="row">
                            <div class="col">
                                <div class="row p-2">
                                    <div class="col"></div>
                                    <div class="col text-right">
                                        <a href="{{ route('page.pay', ['id' => $project_ID]) }}" class="btn btn-sm btn-info"><i class="far fa-eye"></i>&nbsp;&nbsp;檢視支付頁面</a>
                                        <button onclick="createCashGift()" class="btn btn-sm btn-info"><i class="fas fa-plus"></i>&nbsp;&nbsp;新增資料</button>
                                    </div>
                                </div>

                                <table class="table table-hover" id="cashGiftList">
                                    <thead>
                                        <tr>
                                            <th width="10%" class="text-center">#</th>
                                            <th width="20%" class="text-center">姓名</th>
                                            <th width="25%" class="text-center">金額</th>
                                            <th width="25%" class="text-center">交易序號(線上支付)</th>
                                            <th width="20%" class="text-center" class="text-center">操作</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-center" colspan="6">取得資料中...</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="project-management" role="tabpanel" aria-labelledby="project-management-tab">
                        <div class="row">
                            <div class="col-12 col-lg-5 col-md-6 col-sm-12 col-xs-12 py-2 px-3">
                                <div class="form-group mb-3">
                                    <label>專案名稱<span class="text-danger">*</span></label>
                                    <input type="text" id="name" class="form-control" placeholder="輸入專案名稱" autocomplete="off">
                                    <div class="invalid-feedback" id="name-invalid-message"></div>
                                </div>

                                <div class="form-group mb-3">
                                    <label>描述<span class="text-danger">*</span></label>
                                    <textarea id="description" class="form-control"></textarea>
                                    <div class="invalid-feedback" id="description-invalid-message"></div>
                                </div>

                                <div class="text-right mt-2">
                                    <button class="btn btn-sm btn-success px-4" id="saveBtn" onclick="saveProject()">儲 存</button>
                                </div>
                            </div>
                        </div>

                        <hr>
                        <div class="text-center pb-3">
                            <button class="btn btn-sm btn-outline-danger px-4" id="saveBtn" onclick="removeProject()">刪 除 專 案</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    const project_ID = "{{ $project_ID }}";
    const getProjectInfoAPI = "{{ route('api.account.project.searchByID') }}";
    const updateProjectAPI = "{{ route('api.account.project.update') }}";
    const deleteProjectAPI = "{{ route('api.account.project.delete') }}";

    const getFormDescriptionAPI = "{{ route('api.account.form.description.searchByProjectID') }}";
    const updateFormDescriptionAPI = "{{ route('api.account.form.description.update') }}";

    const getGuestTypeAPI = "{{ route('api.account.form.guestType.searchByProjectID') }}";
    const createGuestTypeAPI = "{{ route('api.account.form.guestType.create') }}";
    const updateGuestTypeAPI = "{{ route('api.account.form.guestType.update') }}";
    const deleteGuestTypeAPI = "{{ route('api.account.form.guestType.delete') }}";

    const getGuestAttendAPI = "{{ route('api.account.form.guestAttend.searchByProjectID') }}";
    const deleteGuestAttendAPI = "{{ route('api.account.form.guestAttend.delete') }}";

    const getCashGiftAPI = "{{ route('api.account.form.cashGift.searchByProjectID') }}";
    const createCashGiftAPI = "{{ route('api.account.form.cashGift.create') }}";
    const updateCashGiftAPI = "{{ route('api.account.form.cashGift.update') }}";
    const deleteCashGiftAPI = "{{ route('api.account.form.cashGift.delete') }}";

    const projectIndexRoute = "{{ route('console.project') }}";
</script>

<script>
    // 禮金簿
    function removeCashGift(cashGiftID) {
        $.confirm({
            title: '刪除禮金',
            type: 'orange',
            content: '刪除禮金後將無法恢復，是否確定刪除?',
            buttons: {
                remove: {
                    text: '刪 除',
                    btnClass: 'btn btn-danger',
                    action: function() {
                        data = {
                            'id': cashGiftID,
                            '_method': 'delete',
                        };
                        deleteCashGiftRequest(data);
                    }
                },
                cancel: {
                    text: '取 消'
                }
            }
        });
    }

    function deleteCashGiftRequest(data) {
        $.ajax({
            url: deleteCashGiftAPI,
            type: 'post',
            data: data
        }).done(function(response) {
            if (response['status'] == 'success') {
                toast.fire('success', '刪除成功');
                getCashGiftRequest();
            } else if (response['status'] == 'error') {
                toast.fire('error', response['message']);
            } else {
                toast.fire('warning', '無法辨識伺服器回傳資料');
            }
        }).fail(function() {
            toast.fire('warning', '無法向伺服器傳送資料');
        });
    }

    function editCashGift(cashGiftID) {
        const cashGift = $(`#cashGift${cashGiftID}`);

        let content = `
            <div class="form-group">
                <label>姓名<span class="text-danger">*</span></label>
                <input type="text" class="form-control" id="cashGift-name" placeholder="輸入賓客姓名" autocomplete="off" value="${cashGift.attr('name')}">
                <div class="invalid-feedback" id="cashGift-name-invalid-message"></div>
            </div>

            <div class="form-group">
                <label>金額<span class="text-danger">*</span></label>
                <input type="number" min=0 class="form-control" id="amount" placeholder="輸入金額" autocomplete="off" value="${cashGift.attr('amount')}">
                <div class="invalid-feedback" id="amount-invalid-message"></div>
            </div>

            <div class="form-group">
                <label>交易序號</label>
                <input type="text" class="form-control" autocomplete="off" value="${cashGift.attr('orderCode')}" disabled>
            </div>
        `;

        $.confirm({
            title: '編輯資料',
            type: 'blue',
            content: content,
            buttons: {
                save: {
                    text: '儲 存',
                    btnClass: 'btn btn-success',
                    action: function() {
                        const cashGiftName = $('#cashGift-name');
                        const amount = $('#amount');

                        let input_hasNull = false;
                        input_hasNull = input_hasNull || input_isNull(cashGiftName);
                        input_hasNull = input_hasNull || input_isNull(amount);

                        input_is_invalid(cashGiftName);
                        input_is_invalid(amount);

                        if (!input_hasNull) {
                            let data = {
                                'id': cashGiftID,
                                'name': cashGiftName.val(),
                                'amount': amount.val(),
                                '_method': 'put',
                            };
                            updateCashGiftRequest(data);
                        } else {
                            return false;
                        }
                    }
                },
                cancel: {
                    text: '取 消'
                }
            }
        });
    }

    function updateCashGiftRequest(data) {
        console.log(data);
        $.ajax({
            url: updateCashGiftAPI,
            type: 'post',
            data: data
        }).done(function(response) {
            if (response['status'] == 'success') {
                toast.fire('success', '儲存成功');
                getCashGiftRequest();
            } else if (response['status'] == 'error') {
                toast.fire('error', response['message']);
            } else {
                toast.fire('warning', '無法辨識伺服器回傳資料');
            }
        }).fail(function() {
            toast.fire('warning', '無法向伺服器傳送資料');
        });
    }

    function createCashGift() {
        let content = `
            <div class="form-group">
                <label>姓名<span class="text-danger">*</span></label>
                <input type="text" class="form-control" id="cashGift-name" placeholder="輸入賓客姓名" autocomplete="off">
                <div class="invalid-feedback" id="cashGift-name-invalid-message"></div>
            </div>

            <div class="form-group">
                <label>金額<span class="text-danger">*</span></label>
                <input type="number" min=0 class="form-control" id="amount" placeholder="輸入金額" autocomplete="off">
                <div class="invalid-feedback" id="amount-invalid-message"></div>
            </div>
        `;

        $.confirm({
            title: '新增資料',
            type: 'blue',
            content: content,
            buttons: {
                save: {
                    text: '新 增',
                    btnClass: 'btn btn-success',
                    action: function() {
                        const cashGiftName = $('#cashGift-name');
                        const amount = $('#amount');

                        let input_hasNull = false;
                        input_hasNull = input_hasNull || input_isNull(cashGiftName);
                        input_hasNull = input_hasNull || input_isNull(amount);

                        input_is_invalid(cashGiftName);
                        input_is_invalid(amount);

                        if (!input_hasNull) {
                            let data = {
                                'project_ID': project_ID,
                                'name': cashGiftName.val(),
                                'amount': amount.val(),
                                'orderCode': '',
                            };
                            createCashGiftRequest(data);
                        } else {
                            return false;
                        }
                    }
                },
                cancel: {
                    text: '取 消'
                }
            }
        });
    }

    function createCashGiftRequest(data) {
        $.ajax({
            url: createCashGiftAPI,
            type: 'post',
            data: data
        }).done(function(response) {
            if (response['status'] == 'success') {
                toast.fire('success', '新增成功');
                getCashGiftRequest();
            } else if (response['status'] == 'error') {
                toast.fire('error', response['message']);
            } else {
                toast.fire('warning', '無法辨識伺服器回傳資料');
            }
        }).fail(function() {
            toast.fire('warning', '無法向伺服器傳送資料');
        });
    }

    function getCashGiftRequest() {
        $.ajax({
            url: getCashGiftAPI,
            type: 'post',
            data: {
                'id': project_ID,
            }
        }).done(function(response) {
            if (response['status'] == 'success') {
                putCashGift(response['message']);
            } else if (response['status'] == 'error') {
                toast.fire('error', response['message']);
            } else {
                toast.fire('warning', '無法辨識伺服器回傳資料');
            }
        }).fail(function() {
            toast.fire('warning', '無法向伺服器傳送資料');
        });
    }

    function putCashGift(data) {
        let content = '';
        let blanace = 0;
        data.forEach(function(value, index) {
            blanace += value['amount'];
            content += `
                <tr id="cashGift${value['id']}" name="${value['name']}" amount="${value['amount']}" orderCode="${value['orderCode']}">
                    <td class="text-center">${value['id']}</td>
                    <td class="text-center">${value['name']}</td>
                    <td class="text-center">${value['amount']}</td>
                    <td class="text-center">${value['orderCode']}</td>
                    <td class="text-center">
                        <button class="btn btn-sm btn-info" onclick="editCashGift('${value['id']}')"><i class="fas fa-pen"></i></button>
                        <button class="btn btn-sm btn-danger mr-2" onclick="removeCashGift('${value['id']}')"><i class="fas fa-trash"></i></button>
                    </td>
                </tr>
            `;
        });

        $('#cashGiftList tbody').html(content);
        $('#blanace').html(blanace);
    }

    // 賓客出席
    function removeGuestAttend(guestAttendID) {
        $.confirm({
            title: '刪除賓客出席',
            type: 'orange',
            content: '刪除賓客出席後將無法恢復，是否確定刪除?',
            buttons: {
                remove: {
                    text: '刪 除',
                    btnClass: 'btn btn-danger',
                    action: function() {
                        data = {
                            'id': guestAttendID,
                            '_method': 'delete',
                        };
                        deleteGuestAttendRequest(data);
                    }
                },
                cancel: {
                    text: '取 消'
                }
            }
        });
    }

    function deleteGuestAttendRequest(data) {
        $.ajax({
            url: deleteGuestAttendAPI,
            type: 'post',
            data: data
        }).done(function(response) {
            if (response['status'] == 'success') {
                toast.fire('success', '刪除成功');
                getGuestAttendRequest();
            } else if (response['status'] == 'error') {
                toast.fire('error', response['message']);
            } else {
                toast.fire('warning', '無法辨識伺服器回傳資料');
            }
        }).fail(function() {
            toast.fire('warning', '無法向伺服器傳送資料');
        });
    }

    function getGuestAttendRequest() {
        $.ajax({
            url: getGuestAttendAPI,
            type: 'post',
            data: {
                'id' : project_ID,
            }
        }).done(function(response) {
            if (response['status'] == 'success') {
                putGuestAttend(response['message']);
            } else if (response['status'] == 'error') {
                toast.fire('error', response['message']);
            } else {
                toast.fire('warning', '無法辨識伺服器回傳資料');
            }
        }).fail(function() {
            toast.fire('warning', '無法向伺服器傳送資料');
        });
    }

    function putGuestAttend(data) {
        let content = '';
        let people = 0;
        data.forEach(function(value, index) {
            people += (value['attend'] == 'yes') ? value['people']: 0;
            content += `
                <tr>
                    <td class="text-center">${index + 1}</td>
                    <td class="text-center">${value['name']}</td>
                    <td class="text-center">${value['description']}</td>
                    <td class="text-center">${value['attend']}</td>
                    <td class="text-center">${value['people']}</td>
                    <td class="text-center">
                        <button class="btn btn-sm btn-danger mr-2" onclick="removeGuestAttend('${value['id']}')"><i class="fas fa-trash"></i></button>
                    </td>
                </tr>
            `;
        });

        $('#guestAttendList tbody').html(content);
        $('#guestAttendNumber').html(data.length);
        $('#people').html(people);
    }

    // 賓客類別
    function removeGuestType(guestTypeID) {
        $.confirm({
            title: '刪除類別',
            type: 'orange',
            content: '是否確定刪除類別?',
            buttons: {
                remove: {
                    text: '刪 除',
                    btnClass: 'btn btn-danger',
                    action: function() {
                        data = {
                            'id': guestTypeID,
                            '_method': 'delete',
                        };
                        deleteGuestTypeRequest(data);
                    }
                },
                cancel: {
                    text: '取 消'
                }
            }
        });
    }

    function deleteGuestTypeRequest(data) {
        $.ajax({
            url: deleteGuestTypeAPI,
            type: 'post',
            data: data
        }).done(function(response) {
            if (response['status'] == 'success') {
                toast.fire('success', '刪除成功');
                getGuestTypeRequest();
            } else if (response['status'] == 'error') {
                toast.fire('error', response['message']);
            } else {
                toast.fire('warning', '無法辨識伺服器回傳資料');
            }
        }).fail(function() {
            toast.fire('warning', '無法向伺服器傳送資料');
        });
    }

    function editGuestType(guestTypeID) {
        const guestType = $(`#guestType${guestTypeID}`);
        let content = `
            <div class="form-group">
                <label>類別名稱<span class="text-danger">*</span></label>
                <input type="text" class="form-control" id="guestType-description" placeholder="輸入類別名稱" autocomplete="off" value="${guestType.attr('description')}">
                <div class="invalid-feedback" id="guestType-description-invalid-message"></div>
            </div>
        `;

        $.confirm({
            title: '編輯類別',
            type: 'blue',
            content: content,
            buttons: {
                save: {
                    text: '儲 存',
                    btnClass: 'btn btn-success',
                    action: function() {
                        const guestType_description = $('#guestType-description');

                        let input_hasNull = false;
                        input_hasNull = input_hasNull || input_isNull(guestType_description);

                        input_is_invalid(guestType_description);

                        if (!input_hasNull) {
                            let data = {
                                'id' : guestTypeID,
                                'description': guestType_description.val(),
                                '_method': 'put',
                            };
                            updateGuestTypeRequest(data);
                        } else {
                            return false;
                        }
                    }
                },
                cancel: {
                    text: '取 消'
                }
            }
        });
    }

    function updateGuestTypeRequest(data) {
        $.ajax({
            url: updateGuestTypeAPI,
            type: 'post',
            data: data,
        }).done(function(response) {
            if (response['status'] == 'success') {
                toast.fire('success', '儲存成功');
                getGuestTypeRequest();
            } else if (response['status'] == 'error') {
                toast.fire('error', response['message']);
            } else {
                toast.fire('warning', '無法辨識伺服器回傳資料');
            }
        }).fail(function() {
            toast.fire('warning', '無法向伺服器傳送資料');
        });
    }

    function createGuestType() {
        let content = `
            <div class="form-group">
                <label>類別名稱<span class="text-danger">*</span></label>
                <input type="text" class="form-control" id="guestType-description" placeholder="輸入類別名稱" autocomplete="off">
                <div class="invalid-feedback" id="guestType-description-invalid-message"></div>
            </div>
        `;

        $.confirm({
            title: '新增類別',
            type: 'blue',
            content: content,
            buttons: {
                save: {
                    text: '建 立',
                    btnClass: 'btn btn-success',
                    action: function() {
                        const guestType_description = $('#guestType-description');

                        let input_hasNull = false;
                        input_hasNull = input_hasNull || input_isNull(guestType_description);

                        input_is_invalid(guestType_description);

                        if (!input_hasNull) {
                            let data = {
                                'project_ID' : project_ID,
                                'description': guestType_description.val(),
                            };
                            createGuestTypeRequest(data);
                        } else {
                            return false;
                        }
                    }
                },
                cancel: {
                    text: '取 消'
                }
            }
        });
    }

    function createGuestTypeRequest(data) {
        $.ajax({
            url: createGuestTypeAPI,
            type: 'post',
            data: data,
        }).done(function(response) {
            if (response['status'] == 'success') {
                toast.fire('success', '建立成功');
                getGuestTypeRequest();
            } else if (response['status'] == 'error') {
                toast.fire('error', response['message']);
            } else {
                toast.fire('warning', '無法辨識伺服器回傳資料');
            }
        }).fail(function() {
            toast.fire('warning', '無法向伺服器傳送資料');
        });
    }

    function getGuestTypeRequest() {
        $.ajax({
            url: getGuestTypeAPI,
            type: 'post',
            data: {
                'id' : project_ID,
            }
        }).done(function(response) {
            if (response['status'] == 'success') {
                putGuestType(response['message']);
            } else if (response['status'] == 'error') {
                toast.fire('error', response['message']);
            } else {
                toast.fire('warning', '無法辨識伺服器回傳資料');
            }
        }).fail(function() {
            toast.fire('warning', '無法向伺服器傳送資料');
        });
    }

    function putGuestType(data) {
        let content = '';
        data.forEach(function(value, index) {
            content += `
                <tr id="guestType${value['id']}" description="${value['description']}">
                    <td>${value['description']}</td>
                    <td class="text-center">
                        <button class="btn btn-sm btn-info" onclick="editGuestType('${value['id']}')"><i class="fas fa-pen"></i></button>
                        <button class="btn btn-sm btn-danger mr-2" onclick="removeGuestType('${value['id']}')"><i class="fas fa-trash"></i></button>
                    </td>
                </tr>
            `;
        });

        content += `
            <tr>
                <td colspan="2" class="text-center">
                    <button class="btn btn-sm btn-info" onclick="createGuestType()"><i class="fas fa-plus"></i>&nbsp;&nbsp; 新 增 類 別</button>
                </td>
            </tr>
        `;

        $('#guestTypeList tbody').html(content);
    }

    // 表單描述
    function updateFormDescriptionRequest() {
        $.ajax({
            url: updateFormDescriptionAPI,
            type: 'post',
            data: {
                'project_ID' : project_ID,
                'description' : $('#form-description').val(),
                '_method' : 'put',
            }
        }).done(function(response) {
            if (response['status'] == 'success') {
                getFormDescriptionRequest();
            } else if (response['status'] == 'error') {
                toast.fire('error', response['message']);
            } else {
                toast.fire('warning', '無法辨識伺服器回傳資料');
            }
        }).fail(function() {
            toast.fire('warning', '無法向伺服器傳送資料');
        });
    }

    function getFormDescriptionRequest() {
        $.ajax({
            url: getFormDescriptionAPI,
            type: 'post',
            data: {
                'id' : project_ID,
            }
        }).done(function(response) {
            if (response['status'] == 'success') {
                putFormDescription(response['message']);
            } else if (response['status'] == 'error') {
                toast.fire('error', response['message']);
            } else {
                toast.fire('warning', '無法辨識伺服器回傳資料');
            }
        }).fail(function() {
            toast.fire('warning', '無法向伺服器傳送資料');
        });
    }

    function putFormDescription(data) {
        console.log(data);
        $('#form-description').val(data['description']);
    }

    // 專案編輯
    function removeProject() {
        $.confirm({
            title: '刪除專案',
            type: 'orange',
            content: '刪除專案後將無法恢復，是否確定刪除專案?',
            buttons: {
                remove: {
                    text: '刪 除',
                    btnClass: 'btn btn-danger',
                    action: function() {
                        data = {
                            'id': project_ID,
                            '_method': 'delete',
                        };
                        deleteProjectRequest(data);
                    }
                },
                cancel: {
                    text: '取 消'
                }
            }
        });
    }

    function deleteProjectRequest(data) {
        $.ajax({
            url: deleteProjectAPI,
            type: 'post',
            data: data
        }).done(function(response) {
            if (response['status'] == 'success') {
                toast.fire('success', '刪除成功，三秒後跳轉到首頁');
                setTimeout(() => {
                    location.assign(projectIndexRoute);
                }, 3000);
            } else if (response['status'] == 'error') {
                toast.fire('error', response['message']);
            } else {
                toast.fire('warning', '無法辨識伺服器回傳資料');
            }
        }).fail(function() {
            toast.fire('warning', '無法向伺服器傳送資料');
        });
    }

    function saveProject() {
        const name = $('#name');
        const description = $('#description');
        cancel_invalid(name);
        cancel_invalid(description);

        let input_hasNull = false;
        input_hasNull = input_hasNull || input_isNull(name);
        input_hasNull = input_hasNull || input_isNull(description);

        input_is_invalid(name);
        input_is_invalid(description);

        if (!input_hasNull) {
            let data = {
                'id' : project_ID,
                'name': name.val(),
                'description': description.val(),
                '_method' : 'put',
            };
            updateProjectRequest(data);
        }
    }

    function updateProjectRequest(data) {
        $.ajax({
            url: updateProjectAPI,
            type: 'post',
            data: data,
        }).done(function(response) {
            if (response['status'] == 'success') {
                toast.fire('success', '儲存成功');
                getProjectRequest();
            } else if (response['status'] == 'error') {
                toast.fire('error', response['message']);
            } else {
                toast.fire('warning', '無法辨識伺服器回傳資料');
            }
        }).fail(function() {
            toast.fire('warning', '無法向伺服器傳送資料');
        });
    }

    function getProjectRequest() {
        $.ajax({
            url: getProjectInfoAPI,
            type: 'post',
            data: {
                'id' : project_ID,
            },
        }).done(function(response) {
            if (response['status'] == 'success') {
                putProjectInfo(response['message'])
            } else if (response['status'] == 'error') {
                toast.fire('error', response['message']);
            } else {
                toast.fire('warning', '無法辨識伺服器回傳資料');
            }
        }).fail(function() {
            toast.fire('warning', '無法向伺服器傳送資料');
        });
    }

    function putProjectInfo(data) {
        const name = $('#name');
        const description = $('#description');

        name.val(data['name']);
        description.val(data['description']);
    }

    getProjectRequest();
    getFormDescriptionRequest();
    getGuestTypeRequest();
    getGuestAttendRequest();
    getCashGiftRequest();
</script>
@endsection