<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf8">
        <title>iMarry | 登入</title>

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ URL('resources/template/plugins/fontawesome-free/css/all.min.css') }}">

        <!-- Theme style -->
        <link rel="stylesheet" href="{{ URL('resources/template/dist/css/adminlte.min.css') }}">
        <!-- SweetAlert2 -->
        <link rel="stylesheet" href="{{ URL('resources/template/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
        <!-- Toastr -->
        <link rel="stylesheet" href="{{ URL('resources/template/plugins/toastr/toastr.min.css') }}">
        <!-- jQuery -->
        <script src="{{ URL('resources/template/plugins/jquery/jquery.min.js') }}"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="{{ URL('resources/template/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
        <!-- juqery-conform -->
        <link rel="stylesheet" href="{{ URL('resources/static/libraries/jquery-confirm/jquery-confirm.min.css') }}">
        <script src="{{ URL('resources/static/libraries/jquery-confirm/jquery-confirm.min.js') }}"></script>
        <!-- Custom -->
        <link rel="stylesheet" href="{{ URL('resources/static/css/custom.css') }}">
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                i<b>Marry</b>
            </div>
            <div class="card">
                <div class="card-body login-card-body">
                    <div class="input-group mb-3">
                        <input type="text" id="email" class="form-control" placeholder="信箱" autocomplete="off">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                        <div class="invalid-feedback" id="email-invalid-message"></div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="password" id="password" class="form-control" placeholder="密碼">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                        <div class="invalid-feedback" id="password-invalid-message"></div>
                    </div>
                    <button class="btn btn-success btn-sm btn-block" onclick="signin()" id="loginBtn">登 入</button>
                    <div class="text-center my-2"> or </div>
                    <a href="{{ route('console.signup') }}" class="btn btn-info btn-sm btn-block text-white" id="loginBtn">前 往 註 冊</a>
                </div>
            </div>
        </div>
    </body>
    <!-- Bootstrap 4 -->
    <script src="{{ URL('resources/template/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- Sparkline -->
    <script src="{{ URL('resources/template/plugins/sparklines/sparkline.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ URL('resources/template/dist/js/adminlte.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ URL('resources/template/dist/js/demo.js') }}"></script>
    <!-- SweetAlert2 -->
    <script src="{{ URL('resources/template/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <!-- Toastr -->
    <script src="{{ URL('resources/template/plugins/toastr/toastr.min.js') }}"></script>
    <!-- form -->
    <script src="{{ URL('resources/static/js/form.js') }}"></script>
    <!-- alert -->
    <script src="{{ URL('resources/static/js/alert.js') }}"></script>

    <script>
        const signinAPI = "{{ route('api.account.signin') }}";
        const consoleRoute = "{{ route('console.project') }}";
    </script>
    <script>
    async function signin() {
            const email = $('#email');
            const password = $('#password');

            let input_hasNull = false;
            input_hasNull = input_hasNull || input_isNull(email);
            input_hasNull = input_hasNull || input_isNull(password);

            input_is_invalid(email);
            input_is_invalid(password);
            

            let error = 0;
            $('#loginBtn').toggleClass('disabled');
            if (input_hasNull) {
                error ++;
            }

            if ($('input.is-invalid').length > 0) {
                error ++;
            }

            if (error == 0) {
                let data = {
                    'email' : email.val(),
                    'password' : password.val(),
                };

                signinRequest(data);
            } else {
                $('#loginBtn').toggleClass('disabled');
            };
        }

        function signinRequest(data) {
            $.ajax({
                url: signinAPI,
                type: 'post',
                data: data
            }).done(function(response) {
                $('#loginBtn').removeClass('disabled');
                if (response['status'] == 'success') {
                    location.assign(consoleRoute);  
                } else if (response['status'] == 'error') {
                    toast.fire('error', response['message']);
                } else {
                    toast.fire('warning', '無法辨識伺服器回傳資料');
                }
            }).fail(function() {
                toast.fire('warning', '無法向伺服器傳送資料');
            });
        }
    </script>
</html>