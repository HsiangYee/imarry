@extends('console/layout/basic')

@section('title', '個人資料')

@section('breadcrumb')
    <li class="breadcrumb-item active">個人資料</li>
@endsection

@section('content')
    <div class="card card-info card-outline">
        <div class="row">
            <div class="col-12 col-lg-5 col-md-6 col-sm-12 col-xs-12">
                <div class="card-body box-profile">
                    <div class="input-group mb-3">
                        <input type="text" id="name" class="form-control" placeholder="姓名" autocomplete="off" value="{{ $name }}">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-user"></span>
                            </div>
                        </div>
                        <div class="invalid-feedback" id="name-invalid-message"></div>
                    </div>

                    <div class="input-group mb-3">
                        <input type="text" id="email" class="form-control" placeholder="信箱" autocomplete="off" value="{{ $email }}">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                        <div class="invalid-feedback" id="email-invalid-message"></div>
                    </div>

                    <div class="input-group mb-3">
                        <input type="password" id="password" class="form-control" placeholder="重設密碼">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                        <div class="invalid-feedback" id="password-invalid-message"></div>
                    </div>

                    <div class="input-group mb-3">
                        <input type="password" id="checkPassword" class="form-control" placeholder="確認密碼">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                        <div class="invalid-feedback" id="checkPassword-invalid-message"></div>
                    </div>

                    <div class="text-right">
                        <button class="btn btn-sm btn-success px-4" id="saveBtn" onclick="save()">儲 存</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        const accountUpdateAPI = "{{ route('api.account.update') }}";
        const otherEmailVerificationAPI = "{{ route('api.account.otherEmailVerification') }}";
    </script>

    <script>
        async function emailVerification() {
            $.ajax({
                url: otherEmailVerificationAPI,
                type: 'post',
                data: {
                    'email' : $('#email').val()
                }
            }).done(function(response) {
                console.log(response);
                if (response['status'] == 'success') {
                    if ($('#email').hasClass('is-invalid')) {
                        $('#email').removeClass('is-invalid');
                    }

                    return true;
                } else if (response['status'] == 'error') {
                    if (!$('#email').hasClass('is-invalid')) {
                        $('#email').toggleClass('is-invalid');
                        $(`#email-invalid-message`).html('不可使用此信箱');
                    }

                    return false;
                } else {
                    toast.fire('warning', '無法辨識伺服器回傳資料');

                    return false;
                }
            }).fail(function() {
                toast.fire('warning', '無法向伺服器傳送資料')
                return false;
            });
        }

        async function save() {
            const name = $('#name');
            const email = $('#email');
            const password = $('#password');
            const checkPassword = $('#checkPassword');
            cancel_invalid(name);
            cancel_invalid(email);
            cancel_invalid(password);
            cancel_invalid(checkPassword);

            let input_hasNull = false;
            input_hasNull = input_hasNull || input_isNull(name);
            input_hasNull = input_hasNull || input_isNull(email);
            if (password.val() != '') {
                input_hasNull = input_hasNull || input_isNull(password);
                input_hasNull = input_hasNull || input_isNull(checkPassword);
            }

            input_is_invalid(name);
            input_is_invalid(email);
            if (password.val() != '') {
                input_is_invalid(password);
                input_is_invalid(checkPassword);
            }

            let error = 0;
            $('#saveBtn').toggleClass('disabled');
            if (input_hasNull) {
                error ++;
                console.log('1');
            }

            if (!input_check(password, checkPassword)) {
                error ++;
                console.log('2');
            }

            if (await !emailVerification()){
                error ++;
                console.log('3');
            }

            $('#saveBtn').toggleClass('disabled');
            if ($('input.is-invalid').length == 0 && error == 0) {
                let data = {
                    'name' : name.val(),
                    'email' : email.val(),
                    'password' : password.val(),
                    '_method' : 'put',
                };

                accountUpdateRequest(data);
            } 
        }

        function accountUpdateRequest(data) {
            $.ajax({
                url: accountUpdateAPI,
                type: 'post',
                data: data
            }).done(function(response) {
                console.log(response);
                if (response['status'] == 'success') {
                    toast.fire('success', '儲存成功');
                    setTimeout(() => {
                        location.reload();
                    }, 1500);
                } else if (response['status'] == 'error') {
                    toast.fire('error', response['message']);
                } else {
                    toast.fire('warning', '無法辨識伺服器回傳資料');
                }
            }).fail(function() {
                toast.fire('warning', '無法向伺服器傳送資料');
            });
        }
    </script>
@endsection
