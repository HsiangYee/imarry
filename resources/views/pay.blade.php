<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf8">
        <title>iMarry | 禮金支付</title>

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ URL('resources/template/plugins/fontawesome-free/css/all.min.css') }}">

        <!-- Theme style -->
        <link rel="stylesheet" href="{{ URL('resources/template/dist/css/adminlte.min.css') }}">
        <!-- SweetAlert2 -->
        <link rel="stylesheet" href="{{ URL('resources/template/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
        <!-- Toastr -->
        <link rel="stylesheet" href="{{ URL('resources/template/plugins/toastr/toastr.min.css') }}">
        <!-- jQuery -->
        <script src="{{ URL('resources/template/plugins/jquery/jquery.min.js') }}"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="{{ URL('resources/template/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
        <!-- juqery-conform -->
        <link rel="stylesheet" href="{{ URL('resources/static/libraries/jquery-confirm/jquery-confirm.min.css') }}">
        <script src="{{ URL('resources/static/libraries/jquery-confirm/jquery-confirm.min.js') }}"></script>
        <!-- Custom -->
        <link rel="stylesheet" href="{{ URL('resources/static/css/custom.css') }}">
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                i<b>Marry</b>
            </div>
            <div class="card">
                <div class="card-body login-card-body">
                    <form id="form" action="{{ route('page.ecpay') }}" method="POST">
                        <input type="hidden" name="project_ID" value="{{ $project_ID }}">
                    
                        <div class="form-group mb-3">
                            <label>姓名<span class="text-danger">*</span></label>
                            <input type="text" id="name" name="name" class="form-control" placeholder="輸入姓名" autocomplete="off">
                            <div class="invalid-feedback" id="name-invalid-message"></div>
                        </div>

                        <div class="form-group mb-3">
                            <label>金額<span class="text-danger">*</span></label>
                            <input type="number" id="amount" name="amount" min=1 class="form-control" placeholder="輸入金額" autocomplete="off">
                            <div class="invalid-feedback" id="amount-invalid-message"></div>
                        </div>

                        <button type="button" onclick="submitForm()" class="btn btn-success btn-sm btn-block" id="loginBtn">產 生 支 付 頁 面</button>
                    </form>
                </div>
            </div>
        </div>
    </body>
    <!-- Bootstrap 4 -->
    <script src="{{ URL('resources/template/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- Sparkline -->
    <script src="{{ URL('resources/template/plugins/sparklines/sparkline.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ URL('resources/template/dist/js/adminlte.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ URL('resources/template/dist/js/demo.js') }}"></script>
    <!-- SweetAlert2 -->
    <script src="{{ URL('resources/template/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <!-- Toastr -->
    <script src="{{ URL('resources/template/plugins/toastr/toastr.min.js') }}"></script>
    <!-- form -->
    <script src="{{ URL('resources/static/js/form.js') }}"></script>
    <!-- alert -->
    <script src="{{ URL('resources/static/js/alert.js') }}"></script>

    <script>
        const payFormRoute = "{{ route('page.ecpay') }}";
    </script>
    <script>
        function submitForm() {
            const name = $('#name');
            const amount = $('#amount');

            let input_hasNull = false;
            input_hasNull = input_hasNull || input_isNull(name);
            input_hasNull = input_hasNull || input_isNull(amount);

            input_is_invalid(name);
            input_is_invalid(amount);

            if (!input_hasNull) {
                $('#form').submit();
            }
        }
    </script>
</html>