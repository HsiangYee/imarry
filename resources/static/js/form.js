function input_isNull(input) {
    let isNull = false;
    if (input.val().trim().length == 0) {
        isNull = true;
    }

    return isNull;
}

function input_is_invalid(input) {
    let id = input.attr('id');
    $(`#${id}-invalid-message`).html('不得空白');
    if (input.val().trim().length == 0 && !input.hasClass('is-invalid')) {
        input.toggleClass('is-invalid');

    } else if (input.val().trim().length != 0 && input.hasClass('is-invalid')) {
        input.removeClass('is-invalid');
    }
}

function input_check(input, input2) {
    if (input.val() != input2.val()) {
        if (!input2.hasClass('is-invalid')) {
            input2.toggleClass('is-invalid');
            $(`#${input2.attr('id')}-invalid-message`).html('確認密碼不同');
        }
        return false;

    } else if (input.val() == input2.val()) {
        if (input2.hasClass('is-invalid')) {
            input2.removeClass('is-invalid');
        }
        
        return true;
    }
}

function invalid_message(input, message) {
    let id = input.attr('id');
    $(`#${id}-invalid-message`).html(message);
    if (!input.hasClass('is-invalid')) {
        input.toggleClass('is-invalid');
    }
}

function cancel_invalid(input) {
    let id = input.attr('id');
    $(`#${id}-invalid-message`).html("");
    if (input.hasClass('is-invalid')) {
        input.removeClass('is-invalid');
    }
}