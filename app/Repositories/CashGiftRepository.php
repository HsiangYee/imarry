<?php

namespace App\Repositories;

/** models */
use App\Models\CashGift;

/** libraries */
use Hash;
use Session;
use Exception;
use Illuminate\Support\Facades\Log;

class CashGiftRepository
{
    /**
     * 建立禮金
     * 
     * @param Object $data 建立資料
     * 
     * @return JSON
     */
    public function create(Object $data)
    {
        try {
            $cashGift = new CashGift();
            $cashGift->project_ID = $data->project_ID;
            $cashGift->name = $data->name;
            $cashGift->amount = $data->amount;
            $cashGift->orderCode = $data->orderCode;
            $status = $cashGift->save();

            if (!$status) {
                throw new Exception('建立時發生問題');
            }

            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            Log::error('CashGiftRepository::create '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 更新禮金
     * 
     * @param Object $data 更新資料
     * 
     * @return JSON
     */
    public function update(Object $data)
    {
        try {
            $cashGift = new CashGift();
            $cashGift = $cashGift->find($data->id);
            $cashGift->name = $data->name;
            $cashGift->amount = $data->amount;
            $status = $cashGift->save();

            if (!$status) {
                throw new Exception('建立時發生問題');
            }

            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            Log::error('CashGiftRepository::update '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 刪除禮金
     * 
     * @param Integer $id 禮金 ID
     * 
     * @return JSON
     */
    public function delete(int $id)
    {
        try {
            $cashGift = new CashGift();
            $cashGift = $cashGift->find($id);
            $status = $cashGift->delete();

            if (!$status) {
                throw new Exception('刪除時發生問題');
            }

            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            Log::error('CashGiftRepository::delete '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 使用 ID 判斷是否存在
     * 
     * @param Integer $id 資料ID
     * 
     * @return JSON
     */
    public function existsByID(Int $id)
    {
        try {
            $cashGift = new CashGift();
            $cashGift = $cashGift->where('id', $id);
            $exists = $cashGift->exists();

            $response = array(
                'status' => 'success',
                'message' => $exists,
            );
        } catch (Exception $e) {
            Log::error('CashGiftRepository::existsByID '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 透過專案 ID 搜尋賓客出席
     * 
     * @param Object $data 搜尋資料
     * 
     * @return JSON
     */
    public function searchByProjectID(Object $data)
    {
        try {
            $guestAttend = new CashGift();
            $guestAttend = $guestAttend->where('project_ID', $data->id)
                    ->select('id', 'name', 'amount', 'orderCode')
                    ->get()
                    ->toArray();

            $response = array(
                'status' => 'success',
                'message' => $guestAttend,
            );
        } catch (Exception $e) {
            Log::error('CashGiftRepository::searchByProjectID '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }
}