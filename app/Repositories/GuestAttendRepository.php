<?php

namespace App\Repositories;

/** models */
use App\Models\GuestAttend;

/** libraries */
use Session;
use Exception;
use Illuminate\Support\Facades\Log;

class GuestAttendRepository
{
    /**
     * 建立賓客出席
     * 
     * @param Object $data 建立資料
     * 
     * @return JSON
     */
    public function create(Object $data)
    {
        try {
            $guestAttend = new GuestAttend();
            $guestAttend->project_ID = $data->project_ID;
            $guestAttend->name = $data->name;
            $guestAttend->guest_type_ID = $data->guest_type_ID;
            $guestAttend->attend = $data->attend;
            $guestAttend->people = $data->people;
            $status = $guestAttend->save();

            if (!$status) {
                throw new Exception('建立時發生問題');
            }

            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            Log::error('GuestAttendRepository::create '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 更新賓客出席
     * 
     * @param Object $data 更新資料
     * 
     * @return JSON
     */
    public function update(Object $data)
    {
        try {
            $guestAttend = new GuestAttend();
            $guestAttend = $guestAttend->find($data->id);
            $guestAttend->name = $data->name;
            $guestAttend->guest_type_ID = $data->guest_type_ID;
            $guestAttend->attend = $data->attend;
            $guestAttend->people = $data->people;
            $status = $guestAttend->save();

            if (!$status) {
                throw new Exception('更新時發生問題');
            }

            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            Log::error('GuestAttendRepository::update '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 刪除賓客出席
     * 
     * @param Integer $id 賓客出席 ID
     * 
     * @return JSON
     */
    public function delete(int $id)
    {
        try {
            $guestAttend = new GuestAttend();
            $guestAttend = $guestAttend->find($id);
            $status = $guestAttend->delete();

            if (!$status) {
                throw new Exception('刪除時發生問題');
            }

            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            Log::error('GuestAttendRepository::delete '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 使用 ID 判斷是否存在
     * 
     * @param Integer $id 資料ID
     * 
     * @return JSON
     */
    public function existsByID(Int $id)
    {
        try {
            $guestAttend = new GuestAttend();
            $guestAttend = $guestAttend->where('id', $id);
            $exists = $guestAttend->exists();

            $response = array(
                'status' => 'success',
                'message' => $exists,
            );
        } catch (Exception $e) {
            Log::error('GuestAttendRepository::existsByID '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 透過專案 ID 搜尋賓客出席
     * 
     * @param Object $data 搜尋資料
     * 
     * @return JSON
     */
    public function searchByProjectID(Object $data)
    {
        try {
            $guestAttend = new GuestAttend();
            $guestAttend = $guestAttend->join('guest_type', 'guest_attend.guest_type_ID', 'guest_type.id')
                    ->where('guest_attend.project_ID', $data->id)
                    ->select('guest_attend.id', 'guest_attend.name', 'guest_type.description', 'guest_attend.attend', 'guest_attend.people')
                    ->get()
                    ->toArray();

            $response = array(
                'status' => 'success',
                'message' => $guestAttend,
            );
        } catch (Exception $e) {
            Log::error('GuestAttendRepository::searchByProjectID '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }
}