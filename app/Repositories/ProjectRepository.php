<?php

namespace App\Repositories;

/** models */
use App\Models\Project;

/** libraries */
use Session;
use Exception;
use Illuminate\Support\Facades\Log;

class ProjectRepository
{
    /**
     * 建立專案
     * 
     * @param Object $data 建立資料
     * 
     * @return JSON
     */
    public function create(Object $data)
    {
        try {
            $project = new Project();
            $project->name = $data->name;
            $project->description = $data->description;
            $project->account_ID = $data->account_ID;
            $status = $project->save();

            if (!$status) {
                throw new Exception('建立時發生問題');
            }

            $response = array(
                'status' => 'success',
                'message' => $project->id,
            );
        } catch (Exception $e) {
            Log::error('ProjectRepository::create '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 更新專案
     * 
     * @param Object $data 更新資料
     * 
     * @return JSON
     */
    public function update(Object $data)
    {
        try {
            $project = new Project();
            $project = $project->find($data->id);
            $project->name = $data->name;
            $project->description = $data->description;
            $status = $project->save();

            if (!$status) {
                throw new Exception('更新時發生問題');
            }

            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            Log::error('ProjectRepository::update '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 刪除專案
     * 
     * @param Integer $id 專案 ID
     * 
     * @return JSON
     */
    public function delete(int $id)
    {
        try {
            $project = new Project();
            $project = $project->find($id);
            $status = $project->delete();

            if (!$status) {
                throw new Exception('刪除時發生問題');
            }

            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            Log::error('ProjectRepository::delete '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 使用 ID 判斷是否存在
     * 
     * @param Integer $id 資料ID
     * 
     * @return JSON
     */
    public function existsByID(Int $id)
    {
        try {
            $project = new Project();
            $project = $project->where('id', $id);
            $exists = $project->exists();

            $response = array(
                'status' => 'success',
                'message' => $exists,
            );
        } catch (Exception $e) {
            Log::error('ProjectRepository::existsByID '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 搜尋專案
     * 
     * @param Object $data 搜尋資料
     * 
     * @return JSON
     */
    public function search(Object $data)
    {
        try {
            $project = new Project();
            $project = $project->where([
                        ['name', 'like', '%'.$data->name.'%'],
                        ['account_ID', '=', $data->account_ID]
                    ])->select('id', 'name', 'description')
                    ->orderBy('id', 'DESC')
                    ->get()
                    ->toArray();

            $response = array(
                'status' => 'success',
                'message' => $project,
            );
        } catch (Exception $e) {
            Log::error('ProjectRepository::search '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 透過 ID 搜尋專案
     * 
     * @param Object $data 搜尋資料
     * 
     * @return JSON
     */
    public function searchByID(Object $data)
    {
        try {
            $project = new Project();
            $project = $project->where([
                        ['id', '=', $data->id],
                        ['account_ID', '=', $data->account_ID]
                    ])->select('id', 'name', 'description')
                    ->first()
                    ->toArray();

            $response = array(
                'status' => 'success',
                'message' => $project,
            );
        } catch (Exception $e) {
            Log::error('ProjectRepository::search '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }
}