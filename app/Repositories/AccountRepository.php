<?php

namespace App\Repositories;

/** models */
use App\Models\Account;

/** libraries */
use Hash;
use Session;
use Exception;
use Illuminate\Support\Facades\Log;

class AccountRepository
{
    /**
     * 建立使用者
     * 
     * @param Object $data 建立資料
     * 
     * @return JSON
     */
    public function create(Object $data)
    {
        try {
            $account = new Account();
            $account->name = $data->name;
            $account->email = $data->email;
            $account->password = Hash::make($data->password);
            $status = $account->save();

            if (!$status) {
                throw new Exception('建立時發生問題');
            }

            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            Log::error('AccountRepository::create '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 更新使用者
     * 
     * @param Object $data 更新資料
     * 
     * @return JSON
     */
    public function update(Object $data)
    {
        try {
            $account = new Account();
            $account = $account->find(Session::get('UID'));
            $account->name = $data->name;
            $account->email = $data->email;
            if ($data->password != '') {
                $account->password = Hash::make($data->password);
            }
            $status = $account->save();

            if (!$status) {
                throw new Exception('建立時發生問題');
            }

            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            Log::error('AccountRepository::update '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 刪除使用者
     * 
     * @param Integer $id 使用者 ID
     * 
     * @return JSON
     */
    public function delete(int $id)
    {
        try {
            $account = new Account();
            $account = $account->find($id);
            $status = $account->delete();

            if (!$status) {
                throw new Exception('刪除時發生問題');
            }

            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            Log::error('AccountRepository::delete '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 使用 ID 判斷是否存在
     * 
     * @param Integer $id 資料ID
     * 
     * @return JSON
     */
    public function existsByID(Int $id)
    {
        try {
            $account = new Account();
            $account = $account->where('id', $id);
            $exists = $account->exists();

            $response = array(
                'status' => 'success',
                'message' => $exists,
            );
        } catch (Exception $e) {
            Log::error('AccountRepository::existsByID '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 使用 email 判斷是否存在
     * 
     * @param String $email 信箱
     * 
     * @return JSON
     */
    public function existsByEmail(String $email)
    {
        try {
            $account = new Account();
            $account = $account->where('email', $email);
            $exists = $account->exists();

            $response = array(
                'status' => 'success',
                'message' => $exists,
            );
        } catch (Exception $e) {
            Log::error('AccountRepository::existsByEmail '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 使用 email 判斷是否存在(不包含自己)
     * 
     * @param Object $data 信箱
     * 
     * @return JSON
     */
    public function existsByOtherEmail(Object $data)
    {
        try {
            $account = new Account();
            $account = $account->where([
                            ['email', '=', $data->email],
                            ['id', '!=', $data->id]
                        ]);
            $exists = $account->exists();

            $response = array(
                'status' => 'success',
                'message' => $exists,
            );
        } catch (Exception $e) {
            Log::error('AccountRepository::existsByEmail '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 使用 email 取得資料
     * 
     * @param String $email 信箱
     * 
     * @return JSON
     */
    public function searchByEmail(String $email)
    {
        try {
            $account = new Account();
            $account = $account->where('email', $email)
                        ->select('id', 'name', 'email', 'password')
                        ->first()->toArray();

            $response = array(
                'status' => 'success',
                'message' => $account,
            );
        } catch (Exception $e) {
            Log::error('AccountRepository::searchByEmail '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }
}