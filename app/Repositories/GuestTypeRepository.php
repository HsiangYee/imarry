<?php

namespace App\Repositories;

/** models */
use App\Models\GuestType;

/** libraries */
use Session;
use Exception;
use Illuminate\Support\Facades\Log;

class GuestTypeRepository
{
    /**
     * 建立賓客類別
     * 
     * @param Object $data 建立資料
     * 
     * @return JSON
     */
    public function create(Object $data)
    {
        try {
            $guestType = new GuestType();
            $guestType->project_ID = $data->project_ID;
            $guestType->description = $data->description;
            $status = $guestType->save();

            if (!$status) {
                throw new Exception('建立時發生問題');
            }

            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            Log::error('GuestTypeRepository::create '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 更新賓客類別
     * 
     * @param Object $data 更新資料
     * 
     * @return JSON
     */
    public function update(Object $data)
    {
        try {
            $guestType = new GuestType();
            $guestType = $guestType->find($data->id);
            $guestType->description = $data->description;
            $status = $guestType->save();

            if (!$status) {
                throw new Exception('更新時發生問題');
            }

            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            Log::error('GuestTypeRepository::update '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 刪除賓客類別
     * 
     * @param Integer $id 賓客類別 ID
     * 
     * @return JSON
     */
    public function delete(int $id)
    {
        try {
            $guestType = new GuestType();
            $guestType = $guestType->find($id);
            $status = $guestType->delete();

            if (!$status) {
                throw new Exception('刪除時發生問題');
            }

            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            Log::error('GuestTypeRepository::delete '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 使用 ID 判斷是否存在
     * 
     * @param Integer $id 資料ID
     * 
     * @return JSON
     */
    public function existsByID(Int $id)
    {
        try {
            $guestType = new GuestType();
            $guestType = $guestType->where('id', $id);
            $exists = $guestType->exists();

            $response = array(
                'status' => 'success',
                'message' => $exists,
            );
        } catch (Exception $e) {
            Log::error('GuestTypeRepository::existsByID '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 透過專案 ID 搜尋賓客類別
     * 
     * @param Object $data 搜尋資料
     * 
     * @return JSON
     */
    public function searchByProjectID(Object $data)
    {
        try {
            $guestType = new GuestType();
            $guestType = $guestType->where('project_ID', $data->id)
                    ->select('id', 'description')
                    ->get()
                    ->toArray();

            $response = array(
                'status' => 'success',
                'message' => $guestType,
            );
        } catch (Exception $e) {
            Log::error('GuestTypeRepository::searchByProjectID '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }
}