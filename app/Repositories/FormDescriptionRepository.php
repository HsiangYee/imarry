<?php

namespace App\Repositories;

/** models */
use App\Models\FormDescription;

/** libraries */
use Session;
use Exception;
use Illuminate\Support\Facades\Log;

class FormDescriptionRepository
{
    /**
     * 建立表單描述
     * 
     * @param Object $data 建立資料
     * 
     * @return JSON
     */
    public function create(Object $data)
    {
        try {
            $formDescription = new FormDescription();
            $formDescription->project_ID = $data->project_ID;
            $formDescription->description = $data->description;
            $status = $formDescription->save();

            if (!$status) {
                throw new Exception('建立時發生問題');
            }

            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            Log::error('FormDescriptionRepository::create '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 更新表單描述
     * 
     * @param Object $data 更新資料
     * 
     * @return JSON
     */
    public function updateByProjectID(Object $data)
    {
        try {
            $formDescription = new FormDescription();
            $status = $formDescription->where('project_ID', $data->project_ID)
                            ->update(['description' => $data->description]);

            if (!$status) {
                throw new Exception('更新時發生問題');
            }

            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            Log::error('FormDescriptionRepository::updateByProjectID '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 刪除表單描述
     * 
     * @param Integer $id 表單描述 ID
     * 
     * @return JSON
     */
    public function delete(int $id)
    {
        try {
            $formDescription = new FormDescription();
            $formDescription = $formDescription->find($id);
            $status = $formDescription->delete();

            if (!$status) {
                throw new Exception('刪除時發生問題');
            }

            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            Log::error('FormDescriptionRepository::delete '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 使用 ID 判斷是否存在
     * 
     * @param Integer $id 資料ID
     * 
     * @return JSON
     */
    public function existsByID(Int $id)
    {
        try {
            $formDescription = new FormDescription();
            $formDescription = $formDescription->where('id', $id);
            $exists = $formDescription->exists();

            $response = array(
                'status' => 'success',
                'message' => $exists,
            );
        } catch (Exception $e) {
            Log::error('FormDescriptionRepository::existsByID '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 透過專案 ID 搜尋表單描述
     * 
     * @param Object $data 搜尋資料
     * 
     * @return JSON
     */
    public function searchByProjectID(Object $data)
    {
        try {
            $formDescription = new FormDescription();
            $formDescription = $formDescription->where('project_ID', $data->id)
                    ->select('id', 'description')
                    ->first()
                    ->toArray();

            $response = array(
                'status' => 'success',
                'message' => $formDescription,
            );
        } catch (Exception $e) {
            Log::error('FormDescriptionRepository::searchByProjectID '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }
}