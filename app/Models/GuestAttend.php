<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GuestAttend extends Model
{
    use HasFactory;
    protected $table = 'guest_attend';
}
