<?php

namespace App\Services;

/** Repositories */
use App\Repositories\ProjectRepository;
use App\Repositories\GuestTypeRepository;

/** libraries */
use Session;
use Exception;
use Illuminate\Support\Facades\Log;

class GuestTypeService
{
    /** 
     * @var object $ProjectRepo  專案模型控制
     * @var object $GuestTypeRepo  賓客類別模型控制
     */
    protected $GuestTypeRepo;

    /**
     * 建構子
     * 
     * @param App\Repositories\GuestTypeRepository $GuestTypeRepo 賓客類別模型控制
     * @param App\Repositories\FormDescriptionRepository $FormDescriptionRepo 表單描述模型控制
     * @property App\Repositories\GuestTypeRepository $GuestTypeRepo 賓客類別模型控制
     * @property App\Repositories\FormDescriptionRepository $FormDescriptionRepo 表單描述模型控制
     * 
     * @return Void
     */
    public function __construct(ProjectRepository $ProjectRepo, GuestTypeRepository $GuestTypeRepo)
    {
        $this->ProjectRepo = $ProjectRepo;
        $this->GuestTypeRepo = $GuestTypeRepo;
    }

    /**
     * 建立賓客類別
     * 
     * @param Object $data 建立資料
     * 
     * @return JSON
     */
    public function create(Object $data)
    {
        try {
            $create = $this->GuestTypeRepo->create($data);
            if ($create['status'] == 'error') {
                throw new Exception('Repo 建立時發生問題');
            }
            
            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            Log::error('GuestTypeService::create '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 更新賓客類別
     * 
     * @param Object $data 更新資料
     * 
     * @return JSON
     */
    public function update(Object $data)
    {
        try {
            $update = $this->GuestTypeRepo->update($data);
            if ($update['status'] == 'error') {
                throw new Exception('Repo 更新時發生問題');
            }
            
            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            Log::error('GuestTypeService::create '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 刪除賓客類別
     * 
     * @param Object $data 刪除資料
     * 
     * @return JSON
     */
    public function delete(Object $data)
    {
        try {
            $delete = $this->GuestTypeRepo->delete($data->id);
            if ($delete['status'] == 'error') {
                throw new Exception('Repo 刪除時發生問題');
            }
            
            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            Log::error('GuestTypeService::delete '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }
    
    /**
     * 透過專案 ID 搜尋賓客類別
     * 
     * @param Object $data 搜尋資料
     * 
     * @return JSON
     */
    public function searchByProjectID(Object $data)
    {
        try {
            $exists = $this->ProjectRepo->existsByID($data->id);
            if ($exists['status'] == 'error') {
                throw new Exception('Repo 判斷資料是否存在發生問題');
            }

            $guest = $this->GuestTypeRepo->searchByProjectID($data);
            if ($guest['status'] == 'error') {
                throw new Exception('Repo 搜尋時發生問題');
            }
            
            $response = array(
                'status' => 'success',
                'message' => $guest['message'],
            );
        } catch (Exception $e) {
            Log::error('GuestTypeService::searchByProjectID '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }
}