<?php

namespace App\Services;

/** Repositories */
use App\Repositories\CashGiftRepository;
use App\Repositories\ProjectRepository;

/** libraries */
use Hash;
use Session;
use Exception;
use Illuminate\Support\Facades\Log;

class CashGiftService
{
    /** 
     * @var object $CashGiftRepo  禮金控制
     */
    protected $CashGiftRepo;

    /**
     * 建構子
     * 
     * @param App\Repositories\CashGiftRepository $CashGiftRepo 禮金模型控制
     * @param App\Repositories\ProjectRepository $ProjectRepo 專案模型控制
     * @property App\Repositories\CashGiftRepository $CashGiftRepo 禮金模型控制
     * @property App\Repositories\ProjectRepository $ProjectRepo 專案模型控制
     * 
     * @return Void
     */
    public function __construct(ProjectRepository $ProjectRepo, CashGiftRepository $CashGiftRepo)
    {
        $this->ProjectRepo = $ProjectRepo;
        $this->CashGiftRepo = $CashGiftRepo;
    }

    /**
     * 建立禮金
     * 
     * @param Object $data 建立資料
     * 
     * @return JSON
     */
    public function create(Object $data)
    {
        try {
            $create = $this->CashGiftRepo->create($data);
            if ($create['status'] == 'error') {
                throw new Exception('Repo 建立時發生問題');
            }
            
            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            Log::error('CashGiftService::create '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 更新禮金
     * 
     * @param Object $data 更新資料
     * 
     * @return JSON
     */
    public function update(Object $data)
    {
        try {
            $exists = $this->CashGiftRepo->existsByID($data->id);
            if ($exists['status'] == 'error') {
                throw new Exception('Repo 判斷資料是否存在發生問題');
            }

            if ($exists['status'] == 'success' && !$exists['message']) {
                throw new Exception('禮金資料不存在');
            }

            $update = $this->CashGiftRepo->update($data);
            if ($update['status'] == 'error') {
                throw new Exception('Repo 更新時發生問題');
            }
            
            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            Log::error('TeacherService::create '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 刪除禮金
     * 
     * @param Object $data 刪除資料
     * 
     * @return JSON
     */
    public function delete(Object $data)
    {
        try {
            $delete = $this->CashGiftRepo->delete($data->id);
            if ($delete['status'] == 'error') {
                throw new Exception('Repo 刪除時發生問題');
            }
            
            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            Log::error('CashGiftService::delete '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 透過專案 ID 搜尋禮金
     * 
     * @param Object $data 搜尋資料
     * 
     * @return JSON
     */
    public function searchByProjectID(Object $data)
    {
        try {
            $exists = $this->ProjectRepo->existsByID($data->id);
            if ($exists['status'] == 'error') {
                throw new Exception('Repo 判斷資料是否存在發生問題');
            }

            $guest = $this->CashGiftRepo->searchByProjectID($data);
            if ($guest['status'] == 'error') {
                throw new Exception('Repo 搜尋時發生問題');
            }
            
            $response = array(
                'status' => 'success',
                'message' => $guest['message'],
            );
        } catch (Exception $e) {
            Log::error('CashGiftService::searchByProjectID '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }
}