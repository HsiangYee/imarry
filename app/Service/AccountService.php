<?php

namespace App\Services;

/** Repositories */
use App\Repositories\AccountRepository;

/** libraries */
use Hash;
use Session;
use Exception;
use Illuminate\Support\Facades\Log;

class AccountService
{
    /** 
     * @var object $AccountRepo  系所模型控制
     */
    protected $AccountRepo;

    /**
     * 建構子
     * 
     * @param App\Repositories\AccountRepository $AccountRepo 使用者模型控制
     * @property App\Repositories\AccountRepository $AccountRepo 使用者模型控制
     * 
     * @return Void
     */
    public function __construct(AccountRepository $AccountRepo)
    {
        $this->AccountRepo = $AccountRepo;
    }

    /**
     * 建立使用者
     * 
     * @param Object $data 建立資料
     * 
     * @return JSON
     */
    public function create(Object $data)
    {
        try {
            $exists = $this->AccountRepo->existsByEmail($data->email);
            if ($exists['status'] == 'error') {
                throw new Exception('Repo 判斷資料是否存在發生問題');
            }

            if ($exists['status'] == 'success' && $exists['message']) {
                throw new Exception('Email資料已存在');
            }

            $create = $this->AccountRepo->create($data);
            if ($create['status'] == 'error') {
                throw new Exception('Repo 建立時發生問題');
            }
            
            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            Log::error('AccountService::create '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 更新使用者
     * 
     * @param Object $data 更新資料
     * 
     * @return JSON
     */
    public function update(Object $data)
    {
        try {
            $exists = $this->AccountRepo->existsByID(Session::get('UID'));
            if ($exists['status'] == 'error') {
                throw new Exception('Repo 判斷資料是否存在發生問題');
            }

            if ($exists['status'] == 'success' && !$exists['message']) {
                throw new Exception('使用者資料不存在');
            }

            $tmpData = (Object)array(
                'id' => Session::get('UID'),
                'email' => $data->email,
            );
            $exists = $this->AccountRepo->existsByOtherEmail($tmpData);
            if ($exists['status'] == 'error') {
                throw new Exception('Repo 判斷資料是否存在發生問題');
            }

            if ($exists['status'] == 'success' && $exists['message']) {
                throw new Exception('Email資料已存在');
            }

            $update = $this->AccountRepo->update($data);
            if ($update['status'] == 'error') {
                throw new Exception('Repo 更新時發生問題');
            }

            Session::put('name', $data->name);
            Session::put('email', $data->email);
            if ($data->password != '') {
                Session::put('password', Hash::make($data->password));
            }
            
            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            Log::error('TeacherService::create '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 使用者登入
     * 
     * @param Object $data 登入資料
     * 
     * @return JSON
     */
    public function signin(Object $data)
    {
        try {
            $verification = $this->verification($data);
            if ($verification['status'] == 'error') {
                throw new Exception('驗證帳戶時發生錯誤');
            }

            if (!$verification['message']) {
                throw new Exception('帳號或密碼錯誤');
            }

            $account = $this->AccountRepo->searchByEmail($data->email);
            if ($account['status'] == 'error') {
                throw new Exception('取得帳戶時發生錯誤');
            }

            Session::put('UID', $account['message']['id']);
            Session::put('name', $account['message']['name']);
            Session::put('email', $data->email);
            Session::put('password', $data->password);
            
            $response = array(
                'status' => 'success',
                'message' => true,
            );
        } catch (Exception $e) {
            Log::error('AccountService::create '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * session 登入
     * 
     * @return JSON
     */
    public function sessionSignin()
    {
        try {
            if (!Session::has('email') || !Session::has('password')) {
                throw new Exception('無權使用');
            }

            $data = (Object)array(
                'email' => Session::get('email'),
                'password' => Session::get('password'),
            );

            $verification = $this->verification($data);
            if ($verification['status'] == 'error') {
                throw new Exception('驗證帳戶時發生錯誤');
            }

            if (!$verification['message']) {
                throw new Exception('帳號或密碼錯誤');
            }
            
            $response = array(
                'status' => 'success',
                'message' => true,
            );
        } catch (Exception $e) {
            Log::error('AccountService::sessionSignin '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 驗證帳戶
     * 
     * @param Object $data 登入資料
     * 
     * @return JSON
     */
    public function verification(Object $data)
    {
        try {
            $exists = $this->AccountRepo->existsByEmail($data->email);
            if ($exists['status'] == 'error') {
                throw new Exception('Repo 取得資料時發生問題');
            }

            if (!$exists['message']) {
                throw new Exception('帳號不存在');
            }

            $account = $this->AccountRepo->searchByEmail($data->email);
            if ($account['status'] == 'error') {
                throw new Exception('Repo 取得資料時發生問題');
            }

            if (!Hash::check($data->password, $account['message']['password'])) {
                throw new Exception('密碼錯誤');
            }
            
            $response = array(
                'status' => 'success',
                'message' => true,
            );
        } catch (Exception $e) {
            Log::error('AccountService::verification '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 登出帳戶
     * 
     * @return JSON
     */
    public function signout()
    {
        try {
            Session::flush();
            
            $response = array(
                'status' => 'success',
                'message' => true,
            );
        } catch (Exception $e) {
            Log::error('AccountService::signout '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 驗證信箱
     * 
     * @param String $email 信箱
     * 
     * @return JSON
     */
    public function emailVerification(String $email)
    {
        try {
            $exists = $this->AccountRepo->existsByEmail($email);
            if ($exists['status'] == 'error') {
                throw new Exception('Repo 取得資料時發生問題');
            }

            if ($exists['message']) {
                throw new Exception('帳號已存在');
            }
            
            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            Log::error('AccountService::emailVerification '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 驗證信箱(不包含自己)
     * 
     * @param String $email 信箱
     * 
     * @return JSON
     */
    public function otherEmailVerification(String $email)
    {
        try {
            $tmpData = (Object)array(
                'id' => Session::get('UID'),
                'email' => $email,
            );
            $exists = $this->AccountRepo->existsByOtherEmail($tmpData);
            if ($exists['status'] == 'error') {
                throw new Exception('Repo 取得資料時發生問題');
            }

            if ($exists['message']) {
                throw new Exception('帳號已存在');
            }
            
            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            Log::error('AccountService::otherEmailVerification '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 取得使用者 Session
     * 
     * @return JSON
     */
    public function accountInfo()
    {
        try {
            $accountInfo = array(
                'name' => Session::get('name'),
                'email' => Session::get('email')
            );
            
            $response = array(
                'status' => 'success',
                'message' => $accountInfo,
            );
        } catch (Exception $e) {
            Log::error('AccountService::accountInfo '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }
}