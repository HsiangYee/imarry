<?php

namespace App\Services;

/** Repositories */
use App\Repositories\ProjectRepository;
use App\Repositories\AccountRepository;
use App\Repositories\FormDescriptionRepository;
use App\Repositories\GuestTypeRepository;
/** libraries */
use Session;
use Exception;
use Illuminate\Support\Facades\Log;

class ProjectService
{
    /** 
     * @var object $ProjectRepo  專案模型控制
     * @var object $AccountRepo  使用者模型控制
     */
    protected $ProjectRepo;

    /**
     * 建構子
     * 
     * @param App\Repositories\ProjectRepository $ProjectRepo 專案模型控制
     * @param App\Repositories\AccountRepository $AccountRepo 使用者模型控制
     * @param App\Repositories\FormDescriptionRepository $FormDescriptionRepo 表單描述模型控制
     * @param App\Repositories\GuestTypeRepository $GuestTypeRepo 賓客出席模型控制
     * @property App\Repositories\ProjectRepository $ProjectRepo 專案模型控制
     * @property App\Repositories\AccountRepository $AccountRepo 使用者模型控制
     * @property App\Repositories\FormDescriptionRepository $FormDescriptionRepo 表單描述模型控制
     * @property App\Repositories\GuestTypeRepository $GuestTypeRepo 賓客出席模型控制
     * 
     * @return Void
     */
    public function __construct(ProjectRepository $ProjectRepo, AccountRepository $AccountRepo, FormDescriptionRepository $FormDescriptionRepo, 
    GuestTypeRepository $GuestTypeRepo)
    {
        $this->ProjectRepo = $ProjectRepo;
        $this->AccountRepo = $AccountRepo;
        $this->FormDescriptionRepo = $FormDescriptionRepo;
        $this->GuestTypeRepo = $GuestTypeRepo;
    }

    /**
     * 建立專案
     * 
     * @param Object $data 建立資料
     * 
     * @return JSON
     */
    public function create(Object $data)
    {
        try {
            $data->account_ID = Session::get('UID');
            $project = $this->ProjectRepo->create($data);
            if ($project['status'] == 'error') {
                throw new Exception('Repo 建立時發生問題');
            }

            $tmp = (Object)array(
                'project_ID' => $project['message'],
                'description' => '出席表單說明',
            );
            $create = $this->FormDescriptionRepo->create($tmp);
            if ($create['status'] == 'error') {
                throw new Exception('Repo 建立時發生問題');
            }

            $tmp = (Object)array(
                'project_ID' => $project['message'],
                'description' => '男方親戚',
            );
            $create = $this->GuestTypeRepo->create($tmp);
            if ($create['status'] == 'error') {
                throw new Exception('Repo 建立時發生問題');
            }

            $tmp = (Object)array(
                'project_ID' => $project['message'],
                'description' => '女方親戚',
            );
            $create = $this->GuestTypeRepo->create($tmp);
            if ($create['status'] == 'error') {
                throw new Exception('Repo 建立時發生問題');
            }
            
            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            Log::error('ProjectService::create '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 更新專案
     * 
     * @param Object $data 更新資料
     * 
     * @return JSON
     */
    public function update(Object $data)
    {
        try {
            $data->account_ID = Session::get('UID');
            $update = $this->ProjectRepo->update($data);
            if ($update['status'] == 'error') {
                throw new Exception('Repo 更新時發生問題');
            }
            
            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            Log::error('ProjectService::create '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 刪除專案
     * 
     * @param Object $data 刪除資料
     * 
     * @return JSON
     */
    public function delete(Object $data)
    {
        try {
            $delete = $this->ProjectRepo->delete($data->id);
            if ($delete['status'] == 'error') {
                throw new Exception('Repo 刪除時發生問題');
            }
            
            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            Log::error('ProjectService::delete '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 搜尋專案
     * 
     * @param Object $data 搜尋資料
     * 
     * @return JSON
     */
    public function search(Object $data)
    {
        try {
            $data->account_ID = Session::get('UID');
            $project = $this->ProjectRepo->search($data);
            if ($project['status'] == 'error') {
                throw new Exception('Repo 搜尋時發生問題');
            }

            foreach($project['message'] as $key => $value) {
                $project['message'][$key]['info_route'] = route('console.project_info', ['id' => $value['id']]);
            }
            
            $response = array(
                'status' => 'success',
                'message' => $project['message'],
            );
        } catch (Exception $e) {
            Log::error('ProjectService::search '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 透過 ID 搜尋專案
     * 
     * @param Object $data 搜尋資料
     * 
     * @return JSON
     */
    public function searchByID(Object $data)
    {
        try {
            $exists = $this->ProjectRepo->existsByID($data->id);
            if ($exists['status'] == 'error') {
                throw new Exception('Repo 判斷資料是否存在發生問題');
            }

            $data->account_ID = Session::get('UID');
            $project = $this->ProjectRepo->searchByID($data);
            if ($project['status'] == 'error') {
                throw new Exception('Repo 搜尋時發生問題');
            }
            
            $response = array(
                'status' => 'success',
                'message' => $project['message'],
            );
        } catch (Exception $e) {
            Log::error('ProjectService::searchByID '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }

    /**
     * 利用 ID 判斷專案是否存在
     * 
     * @param Integer $id 學院 ID
     * 
     * @return JSON
     */
    public function existsByID(Int $id)
    {
        try {
            $exists = $this->ProjectRepo->existsByID($id);
            if ($exists['status'] == 'error') {
                throw new Exception('Repo 判斷資料是否存在發生問題');
            }

            $response = array(
                'status' => 'success',
                'message' => $exists['message'],
            );
        } catch (Exception $e) {
            Log::error('ProjectService::delete '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }
}