<?php

namespace App\Services;

/** Repositories */
use App\Repositories\ProjectRepository;
use App\Repositories\FormDescriptionRepository;

/** libraries */
use Session;
use Exception;
use Illuminate\Support\Facades\Log;

class FormDescriptionService
{
    /** 
     * @var object $ProjectRepo  專案模型控制
     * @var object $FormDescriptionRepo  表單描述模型控制
     */
    protected $FormDescriptionRepo;

    /**
     * 建構子
     * 
     * @param App\Repositories\FormDescriptionRepository $FormDescriptionRepo 表單描述模型控制
     * @param App\Repositories\ProjectRepository $ProjectRepo 專案模型控制
     * @property App\Repositories\FormDescriptionRepository $FormDescriptionRepo 表單描述模型控制
     * @property App\Repositories\ProjectRepository $ProjectRepo 專案模型控制
     * 
     * @return Void
     */
    public function __construct(ProjectRepository $ProjectRepo, FormDescriptionRepository $FormDescriptionRepo)
    {
        $this->ProjectRepo = $ProjectRepo;
        $this->FormDescriptionRepo = $FormDescriptionRepo;
    }


    /**
     * 更新表單描述
     * 
     * @param Object $data 更新資料
     * 
     * @return JSON
     */
    public function updateByProjectID(Object $data)
    {
        try {
            $update = $this->FormDescriptionRepo->updateByProjectID($data);
            if ($update['status'] == 'error') {
                throw new Exception('Repo 更新時發生問題');
            }
            
            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            Log::error('FormDescriptionService::create '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }
    
    /**
     * 透過專案 ID 搜尋表單描述
     * 
     * @param Object $data 搜尋資料
     * 
     * @return JSON
     */
    public function searchByProjectID(Object $data)
    {
        try {
            $exists = $this->ProjectRepo->existsByID($data->id);
            if ($exists['status'] == 'error') {
                throw new Exception('Repo 判斷資料是否存在發生問題');
            }

            $formDescription = $this->FormDescriptionRepo->searchByProjectID($data);
            if ($formDescription['status'] == 'error') {
                throw new Exception('Repo 搜尋時發生問題');
            }
            
            $response = array(
                'status' => 'success',
                'message' => $formDescription['message'],
            );
        } catch (Exception $e) {
            Log::error('FormDescriptionService::searchByProjectID '.$e->getMessage());
            $response = array(
                'status' => 'error',
                'message' => '系統內部發生問題',
            );
        }

        return $response;
    }
}