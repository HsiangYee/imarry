<?php

namespace App\Http\Controllers;

/** Service */
use App\Services\AccountService;

/** Libraries */
use Exception;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    /** @var Object $AccountService */
    protected $AccountService;

    /**
     * 建構子
     * 
     * @param App\Services\AccountService $AccountService 班級服務
     * @property App\Services\AccountService $AccountService 班級服務
     * 
     * @return Void
     */
    public function __construct(AccountService $AccountService)
    {
        $this->AccountService = $AccountService;
    }

    /**
     * 使用者註冊
     * 
     * @param Illuminate\Http\Request $request 請求資料
     * 
     * @return Json
     */
    public function signup(Request $request){
        try {
            $has = !$request->has('name');
            $has = $has || !$request->has('email');
            $has = $has || !$request->has('password');
            $has = $has || !$request->has('checkPassword');
            if ($has) {
                throw new Exception('無法接收到資料');
            }

            if(!preg_match("/^[-A-Za-z0-9_]+[-A-Za-z0-9_.]*[@]{1}[-A-Za-z0-9_]+[-A-Za-z0-9_.]*[.]{1}[A-Za-z]{2,5}$/", $request->email)){
                throw new Exception('信箱格式錯誤');
            }
        
            $signin = $this->AccountService->create($request);
            if ($signin['status'] == 'error') {
                throw new Exception('無法註冊');
            }

            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            $response = array(
                'status' => 'error',
                'message' => $e->getMessage(),
            );
            
        }

        return response()
                ->json($response);
    }

    /**
     * 更新使用者
     * 
     * @param Illuminate\Http\Request $request 請求資料
     * 
     * @return Json
     */
    public function update(Request $request){
        try {
            $has = !$request->has('name');
            $has = $has || !$request->has('email');
            $has = $has || !$request->has('password');
            if ($has) {
                throw new Exception('無法接收到資料');
            }

            if(!preg_match("/^[-A-Za-z0-9_]+[-A-Za-z0-9_.]*[@]{1}[-A-Za-z0-9_]+[-A-Za-z0-9_.]*[.]{1}[A-Za-z]{2,5}$/", $request->email)){
                throw new Exception('信箱格式錯誤');
            }
        
            $signin = $this->AccountService->update($request);
            if ($signin['status'] == 'error') {
                throw new Exception('無法更新');
            }

            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            $response = array(
                'status' => 'error',
                'message' => $e->getMessage(),
            );
            
        }

        return response()
                ->json($response);
    }

    /**
     * 使用者登入
     * 
     * @param Illuminate\Http\Request $request 請求資料
     * 
     * @return Json
     */
    public function signin(Request $request){
        try {
            $has = !$request->has('email');
            $has = $has || !$request->has('password');
            if ($has) {
                throw new Exception('無法接收到資料');
            }

            if(!preg_match("/^[-A-Za-z0-9_]+[-A-Za-z0-9_.]*[@]{1}[-A-Za-z0-9_]+[-A-Za-z0-9_.]*[.]{1}[A-Za-z]{2,5}$/", $request->email)){
                throw new Exception('信箱格式錯誤');
            }
        
            $signin = $this->AccountService->signin($request);
            if ($signin['status'] == 'error') {
                throw new Exception('信箱或密碼錯誤');
            }

            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            $response = array(
                'status' => 'error',
                'message' => $e->getMessage(),
            );
            
        }

        return response()
                ->json($response);
    }

    /**
     * 使用者登出
     * 
     * @return Json
     */
    public function signout(){
        try {
            $signout = $this->AccountService->signout();
            if ($signout['status'] == 'error') {
                throw new Exception('系統內部發生問題');
            }

            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            $response = array(
                'status' => 'error',
                'message' => $e->getMessage(),
            );
            
        }

        return response()
                ->json($response);
    }

    /**
     * 使用者信箱驗證
     * 
     * @param Illuminate\Http\Request $request 請求資料
     * 
     * @return Json
     */
    public function emailVerification(Request $request){
        try {
            $has = !$request->has('email');
            if ($has) {
                throw new Exception('無法接收到資料');
            }

            if(!preg_match("/^[-A-Za-z0-9_]+[-A-Za-z0-9_.]*[@]{1}[-A-Za-z0-9_]+[-A-Za-z0-9_.]*[.]{1}[A-Za-z]{2,5}$/", $request->email)){
                throw new Exception('信箱格式錯誤');
            }

            $verification = $this->AccountService->emailVerification($request->email);
            if ($verification['status'] == 'error') {
                throw new Exception('信箱已被註冊');
            }

            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            $response = array(
                'status' => 'error',
                'message' => $e->getMessage(),
            );
            
        }

        return response()
                ->json($response);
    }

    /**
     * 使用者(不包含自己)信箱驗證
     * 
     * @param Illuminate\Http\Request $request 請求資料
     * 
     * @return Json
     */
    public function otherEmailVerification(Request $request){
        try {
            $has = !$request->has('email');
            if ($has) {
                throw new Exception('無法接收到資料');
            }

            if(!preg_match("/^[-A-Za-z0-9_]+[-A-Za-z0-9_.]*[@]{1}[-A-Za-z0-9_]+[-A-Za-z0-9_.]*[.]{1}[A-Za-z]{2,5}$/", $request->email)){
                throw new Exception('信箱格式錯誤');
            }

            $verification = $this->AccountService->otherEmailVerification($request->email);
            if ($verification['status'] == 'error') {
                throw new Exception('信箱已被註冊');
            }

            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            $response = array(
                'status' => 'error',
                'message' => $e->getMessage(),
            );
            
        }

        return response()
                ->json($response);
    }

    /**
     * 取得使用者資料
     * 
     * @return Json
     */
    public function accountInfo(){
        try {
            $accountInfo = $this->AccountService->accountInfo();
            if ($accountInfo['status'] == 'error') {
                throw new Exception('取得資料時發生問題');
            }

            $response = array(
                'status' => 'success',
                'message' => $accountInfo['message'],
            );
        } catch (Exception $e) {
            $response = array(
                'status' => 'error',
                'message' => $e->getMessage(),
            );
            
        }

        return response()
                ->json($response);
    }
}
