<?php

namespace App\Http\Controllers;

/** Service */
use App\Services\FormDescriptionService;

/** Libraries */
use Exception;
use Illuminate\Http\Request;

class FormDescriptionController extends Controller
{
    /** @var Object $FormDescriptionService */
    protected $FormDescriptionService;

    /**
     * 建構子
     * 
     * @param App\Services\FormDescriptionService $FormDescriptionService 表單描述服務
     * @property App\Services\FormDescriptionService $FormDescriptionService 表單描述服務
     * 
     * @return Void
     */
    public function __construct(FormDescriptionService $FormDescriptionService)
    {
        $this->FormDescriptionService = $FormDescriptionService;
    }

    /**
     * 更新表單描述
     * 
     * @param Illuminate\Http\Request $request 請求資料
     * 
     * @return Json
     */
    public function update(Request $request){
        try {
            $has = !$request->has('project_ID');
            $has = $has || !$request->has('description');
            if ($has) {
                throw new Exception('無法接收到資料');
            }
        
            $signin = $this->FormDescriptionService->updateByProjectID($request);
            if ($signin['status'] == 'error') {
                throw new Exception('無法更新');
            }

            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            $response = array(
                'status' => 'error',
                'message' => $e->getMessage(),
            );
            
        }

        return response()
                ->json($response);
    }

    /**
     * 透過專案 ID 搜尋表單描述
     * 
     * @param Illuminate\Http\Request $request 請求資料
     * 
     * @return Json
     */
    public function searchByProjectID(Request $request){
        try {
            $search = $this->FormDescriptionService->searchByProjectID($request);
            if ($search['status'] == 'error') {
                throw new Exception('無法搜尋');
            }

            $response = array(
                'status' => 'success',
                'message' => $search['message'],
            );
        } catch (Exception $e) {
            $response = array(
                'status' => 'error',
                'message' => $e->getMessage(),
            );
            
        }

        return response()
                ->json($response);
    }
}
