<?php

namespace App\Http\Controllers;

/** Service */
use App\Services\ProjectService;

/** Libraries */
use Session;
use Exception;
use Illuminate\Http\Request;

class ConsolePageController extends Controller
{
    /** @var Object $ProjectService */
    protected $ProjectService;

    /**
     * 建構子
     * 
     * @param App\Services\ProjectService $ProjectService 專案服務
     * @property App\Services\ProjectService $ProjectService 專案服務
     * 
     * @return Void
     */
    public function __construct(ProjectService $ProjectService)
    {
        $this->ProjectService = $ProjectService;
    }

    public function signin ()
    {
        return view('console/signin');
    }

    public function signup ()
    {
        return view('console/signup');
    }

    public function index ()
    {
        return view('console/index');
    }

    public function profile ()
    {
        return view('console/profile')
                ->with([
                    'name' => Session::get('name'),
                    'email' => Session::get('email'),
                ]);
    }

    public function project ()
    {
        return view('console/project/index');
    }

    public function project_info ($id)
    {
        try {
            $exists = $this->ProjectService->existsByID($id);
            if (!$exists['message']) {
                throw new Exception('');
            }

            return view('console/project/info')
                    ->with([
                        'project_ID' => $id,
                    ]);
        } catch (Exception $e) {
            return redirect()
                    ->route('console.project');
        }
    }
}
