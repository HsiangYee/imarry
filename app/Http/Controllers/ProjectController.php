<?php

namespace App\Http\Controllers;

/** Service */
use App\Services\ProjectService;

/** Libraries */
use Exception;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    /** @var Object $ProjectService */
    protected $ProjectService;

    /**
     * 建構子
     * 
     * @param App\Services\ProjectService $ProjectService 專案服務
     * @property App\Services\ProjectService $ProjectService 專案服務
     * 
     * @return Void
     */
    public function __construct(ProjectService $ProjectService)
    {
        $this->ProjectService = $ProjectService;
    }

    /**
     * 建立專案
     * 
     * @param Illuminate\Http\Request $request 請求資料
     * 
     * @return Json
     */
    public function create(Request $request){
        try {
            $has = !$request->has('name');
            $has = $has || !$request->has('description');
            if ($has) {
                throw new Exception('無法接收到資料');
            }
        
            $create = $this->ProjectService->create($request);
            if ($create['status'] == 'error') {
                throw new Exception('無法建立');
            }

            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            $response = array(
                'status' => 'error',
                'message' => $e->getMessage(),
            );
            
        }

        return response()
                ->json($response);
    }

    /**
     * 更新專案
     * 
     * @param Illuminate\Http\Request $request 請求資料
     * 
     * @return Json
     */
    public function update(Request $request){
        try {
            $has = !$request->has('id');
            $has = $has || !$request->has('name');
            $has = $has || !$request->has('description');
            if ($has) {
                throw new Exception('無法接收到資料');
            }
        
            $signin = $this->ProjectService->update($request);
            if ($signin['status'] == 'error') {
                throw new Exception('無法更新');
            }

            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            $response = array(
                'status' => 'error',
                'message' => $e->getMessage(),
            );
            
        }

        return response()
                ->json($response);
    }

    /**
     * 刪除專案
     * 
     * @param Illuminate\Http\Request $request 請求資料
     * 
     * @return Json
     */
    public function delete(Request $request){
        try {
            $has = !$request->has('id');
            if ($has) {
                throw new Exception('無法接收到資料');
            }
        
            $signin = $this->ProjectService->delete($request);
            if ($signin['status'] == 'error') {
                throw new Exception('無法刪除');
            }

            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            $response = array(
                'status' => 'error',
                'message' => $e->getMessage(),
            );
            
        }

        return response()
                ->json($response);
    }

    /**
     * 搜尋專案
     * 
     * @param Illuminate\Http\Request $request 請求資料
     * 
     * @return Json
     */
    public function search(Request $request){
        try {
            $search = $this->ProjectService->search($request);
            if ($search['status'] == 'error') {
                throw new Exception('無法搜尋');
            }

            $response = array(
                'status' => 'success',
                'message' => $search['message'],
            );
        } catch (Exception $e) {
            $response = array(
                'status' => 'error',
                'message' => $e->getMessage(),
            );
            
        }

        return response()
                ->json($response);
    }

    /**
     * 透過 ID 搜尋專案
     * 
     * @param Illuminate\Http\Request $request 請求資料
     * 
     * @return Json
     */
    public function searchByID(Request $request){
        try {
            $search = $this->ProjectService->searchByID($request);
            if ($search['status'] == 'error') {
                throw new Exception('無法搜尋');
            }

            $response = array(
                'status' => 'success',
                'message' => $search['message'],
            );
        } catch (Exception $e) {
            $response = array(
                'status' => 'error',
                'message' => $e->getMessage(),
            );
            
        }

        return response()
                ->json($response);
    }
}
