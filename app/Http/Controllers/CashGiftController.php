<?php

namespace App\Http\Controllers;

/** Service */
use App\Services\CashGiftService;

/** Libraries */
use Exception;
use Illuminate\Http\Request;

class CashGiftController extends Controller
{
    /** @var Object $CashGiftService */
    protected $CashGiftService;

    /**
     * 建構子
     * 
     * @param App\Services\CashGiftService $CashGiftService 禮金服務
     * @property App\Services\CashGiftService $CashGiftService 禮金服務
     * 
     * @return Void
     */
    public function __construct(CashGiftService $CashGiftService)
    {
        $this->CashGiftService = $CashGiftService;
    }

    /**
     * 建立禮金
     * 
     * @param Illuminate\Http\Request $request 請求資料
     * 
     * @return Json
     */
    public function create(Request $request){
        try {
            $has = !$request->has('project_ID');
            $has = $has || !$request->has('name');
            $has = $has || !$request->has('amount');
            $has = $has || !$request->has('orderCode');
            if ($has) {
                throw new Exception('無法接收到資料');
            }
        
            $create = $this->CashGiftService->create($request);
            if ($create['status'] == 'error') {
                throw new Exception('無法建立');
            }

            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            $response = array(
                'status' => 'error',
                'message' => $e->getMessage(),
            );
            
        }

        return response()
                ->json($response);
    }

    /**
     * 更新禮金
     * 
     * @param Illuminate\Http\Request $request 請求資料
     * 
     * @return Json
     */
    public function update(Request $request){
        try {
            $has = !$request->has('id');
            $has = $has || !$request->has('name');
            $has = $has || !$request->has('amount');
            if ($has) {
                throw new Exception('無法接收到資料');
            }
        
            $signin = $this->CashGiftService->update($request);
            if ($signin['status'] == 'error') {
                throw new Exception('無法更新');
            }

            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            $response = array(
                'status' => 'error',
                'message' => $e->getMessage(),
            );
            
        }

        return response()
                ->json($response);
    }

    /**
     * 刪除禮金
     * 
     * @param Illuminate\Http\Request $request 請求資料
     * 
     * @return Json
     */
    public function delete(Request $request){
        try {
            $has = !$request->has('id');
            if ($has) {
                throw new Exception('無法接收到資料');
            }
        
            $signin = $this->CashGiftService->delete($request);
            if ($signin['status'] == 'error') {
                throw new Exception('無法刪除');
            }

            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            $response = array(
                'status' => 'error',
                'message' => $e->getMessage(),
            );
            
        }

        return response()
                ->json($response);
    }

    /**
     * 搜尋禮金
     * 
     * @param Illuminate\Http\Request $request 請求資料
     * 
     * @return Json
     */
    public function searchByProjectID(Request $request){
        try {
            $search = $this->CashGiftService->searchByProjectID($request);
            if ($search['status'] == 'error') {
                throw new Exception('無法搜尋');
            }

            $response = array(
                'status' => 'success',
                'message' => $search['message'],
            );
        } catch (Exception $e) {
            $response = array(
                'status' => 'error',
                'message' => $e->getMessage(),
            );
            
        }

        return response()
                ->json($response);
    }
}
