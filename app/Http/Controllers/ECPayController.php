<?php
namespace App\Http\Controllers;

use Session;
use Cache;
use TsaiYiHua\ECPay\Checkout;
use Illuminate\Http\Request;

use TsaiYiHua\ECPay\Collections\CheckoutResponseCollection;
use TsaiYiHua\ECPay\Services\StringService;
use App\Services\CashGiftService;

class ECPayController extends Controller
{
    protected $checkout;
    protected $checkoutResponse;
    protected $CashGiftService;
    public function __construct(Checkout $checkout, CheckoutResponseCollection $checkoutResponse, CashGiftService $CashGiftService)
    {
        $this->checkout = $checkout;
        $this->checkoutResponse = $checkoutResponse;
        $this->CashGiftService = $CashGiftService;
    }

    public function sendOrder(Request $request)
    {
        $formData = [
            'ItemDescription' => $request->name."禮金:".$request->amount,
            'ItemName' => $request->name."禮金:".$request->amount,
            'TotalAmount' => $request->amount,
            'PaymentMethod' => 'ALL', // ALL, Credit, ATM, WebATM
        ];

        Cache::put('pay-project_ID', $request->project_ID);
        Cache::put('pay-name', $request->name);
        Cache::put('pay-amount', $request->amount);

        return $this->checkout->setPostData($formData)->send();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \TsaiYiHua\ECPay\Exceptions\ECPayException
     */
    public function returnUrl(Request $request)
    {
        $serverPost = $request->post();
        $checkMacValue = $request->post('CheckMacValue');
        unset($serverPost['CheckMacValue']);
        $checkCode = StringService::checkMacValueGenerator($serverPost);
        if ($checkMacValue == $checkCode) {
            if (!empty($request->input('redirect'))) {
                return redirect($request->input('redirect'));
            } else {
                $data = $this->checkoutResponse->collectResponse($serverPost);
                $data = (Object)array(
                    'project_ID' => Cache::pull('pay-project_ID'),
                    'name' => Cache::pull('pay-name'),
                    'amount' => Cache::pull('pay-amount'),
                    'orderCode' => $data['MerchantTradeNo']
                );

                $create = $this->CashGiftService->create($data);
                return view('success')
                    ->with([
                        'name' => $data->name,
                        'amount' => $data->amount,
                        'orderCode' => $data->orderCode
                    ]);
            }
        }
    }
}
