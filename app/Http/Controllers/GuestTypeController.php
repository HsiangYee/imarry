<?php

namespace App\Http\Controllers;

/** Service */
use App\Services\GuestTypeService;

/** Libraries */
use Exception;
use Illuminate\Http\Request;

class GuestTypeController extends Controller
{
    /** @var Object $GuestTypeService */
    protected $GuestTypeService;

    /**
     * 建構子
     * 
     * @param App\Services\GuestTypeService $GuestTypeService 賓客類別服務
     * @property App\Services\GuestTypeService $GuestTypeService 賓客類別服務
     * 
     * @return Void
     */
    public function __construct(GuestTypeService $GuestTypeService)
    {
        $this->GuestTypeService = $GuestTypeService;
    }

    /**
     * 建立賓客類別
     * 
     * @param Illuminate\Http\Request $request 請求資料
     * 
     * @return Json
     */
    public function create(Request $request){
        try {
            $has = !$request->has('project_ID');
            $has = $has || !$request->has('description');
            if ($has) {
                throw new Exception('無法接收到資料');
            }
        
            $create = $this->GuestTypeService->create($request);
            if ($create['status'] == 'error') {
                throw new Exception('無法建立');
            }

            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            $response = array(
                'status' => 'error',
                'message' => $e->getMessage(),
            );
            
        }

        return response()
                ->json($response);
    }

    /**
     * 更新賓客類別
     * 
     * @param Illuminate\Http\Request $request 請求資料
     * 
     * @return Json
     */
    public function update(Request $request){
        try {
            $has = !$request->has('id');
            $has = $has || !$request->has('description');
            if ($has) {
                throw new Exception('無法接收到資料');
            }
        
            $signin = $this->GuestTypeService->update($request);
            if ($signin['status'] == 'error') {
                throw new Exception('無法更新');
            }

            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            $response = array(
                'status' => 'error',
                'message' => $e->getMessage(),
            );
            
        }

        return response()
                ->json($response);
    }

    /**
     * 刪除賓客類別
     * 
     * @param Illuminate\Http\Request $request 請求資料
     * 
     * @return Json
     */
    public function delete(Request $request){
        try {
            $has = !$request->has('id');
            if ($has) {
                throw new Exception('無法接收到資料');
            }
        
            $signin = $this->GuestTypeService->delete($request);
            if ($signin['status'] == 'error') {
                throw new Exception('無法刪除');
            }

            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            $response = array(
                'status' => 'error',
                'message' => $e->getMessage(),
            );
            
        }

        return response()
                ->json($response);
    }

    /**
     * 搜尋賓客類別
     * 
     * @param Illuminate\Http\Request $request 請求資料
     * 
     * @return Json
     */
    public function searchByProjectID(Request $request){
        try {
            $search = $this->GuestTypeService->searchByProjectID($request);
            if ($search['status'] == 'error') {
                throw new Exception('無法搜尋');
            }

            $response = array(
                'status' => 'success',
                'message' => $search['message'],
            );
        } catch (Exception $e) {
            $response = array(
                'status' => 'error',
                'message' => $e->getMessage(),
            );
            
        }

        return response()
                ->json($response);
    }
}
