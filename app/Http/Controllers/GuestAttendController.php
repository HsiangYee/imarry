<?php

namespace App\Http\Controllers;

/** Service */
use App\Services\GuestAttendService;

/** Libraries */
use Exception;
use Illuminate\Http\Request;

class GuestAttendController extends Controller
{
    /** @var Object $GuestAttendService */
    protected $GuestAttendService;

    /**
     * 建構子
     * 
     * @param App\Services\GuestAttendService $GuestAttendService 賓客出席服務
     * @property App\Services\GuestAttendService $GuestAttendService 賓客出席服務
     * 
     * @return Void
     */
    public function __construct(GuestAttendService $GuestAttendService)
    {
        $this->GuestAttendService = $GuestAttendService;
    }

    /**
     * 更新賓客出席
     * 
     * @param Illuminate\Http\Request $request 請求資料
     * 
     * @return Json
     */
    public function create(Request $request){
        try {
            $has = !$request->has('project_ID');
            $has = $has || !$request->has('guest_type_ID');
            $has = $has || !$request->has('name');
            $has = $has || !$request->has('attend');
            $has = $has || !$request->has('people');
            if ($has) {
                throw new Exception('無法接收到資料');
            }
        
            $signin = $this->GuestAttendService->create($request);
            if ($signin['status'] == 'error') {
                throw new Exception('無法建立');
            }

            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            $response = array(
                'status' => 'error',
                'message' => $e->getMessage(),
            );
            
        }

        return response()
                ->json($response);
    }

    /**
     * 刪除賓客出席
     * 
     * @param Illuminate\Http\Request $request 請求資料
     * 
     * @return Json
     */
    public function delete(Request $request){
        try {
            $has = !$request->has('id');
            if ($has) {
                throw new Exception('無法接收到資料');
            }
        
            $signin = $this->GuestAttendService->delete($request);
            if ($signin['status'] == 'error') {
                throw new Exception('無法刪除');
            }

            $response = array(
                'status' => 'success',
                'message' => '',
            );
        } catch (Exception $e) {
            $response = array(
                'status' => 'error',
                'message' => $e->getMessage(),
            );
            
        }

        return response()
                ->json($response);
    }

    /**
     * 搜尋賓客出席
     * 
     * @param Illuminate\Http\Request $request 請求資料
     * 
     * @return Json
     */
    public function searchByProjectID(Request $request){
        try {
            $search = $this->GuestAttendService->searchByProjectID($request);
            if ($search['status'] == 'error') {
                throw new Exception('無法搜尋');
            }

            $response = array(
                'status' => 'success',
                'message' => $search['message'],
            );
        } catch (Exception $e) {
            $response = array(
                'status' => 'error',
                'message' => $e->getMessage(),
            );
            
        }

        return response()
                ->json($response);
    }
}
