<?php

namespace App\Http\Controllers;

/** Service */
use App\Services\ProjectService;
use App\Services\FormDescriptionService;
use App\Services\GuestTypeService;

/** Libraries */
use Exception;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /** @var Object $ProjectService */
    protected $ProjectService;
    protected $FormDescriptionService;
    protected $GuestTypeService;

    /**
     * 建構子
     * 
     * @param App\Services\ProjectService $ProjectService 專案服務
     * @param App\Services\FormDescriptionService $FormDescriptionService 表單描述服務
     * @param App\Services\GuestTypeService $GuestTypeService 賓客類別服務
     * @property App\Services\ProjectService $ProjectService 專案服務
     * @property App\Services\FormDescriptionService $FormDescriptionService 表單描述服務
     * @property App\Services\GuestTypeService $GuestTypeService 賓客類別服務
     * 
     * @return Void
     */
    public function __construct(ProjectService $ProjectService, FormDescriptionService $FormDescriptionService, GuestTypeService $GuestTypeService)
    {
        $this->ProjectService = $ProjectService;
        $this->FormDescriptionService = $FormDescriptionService;
        $this->GuestTypeService = $GuestTypeService;
    }

    public function form ($id)
    {
        try {
            $exists = $this->ProjectService->existsByID($id);
            if (!$exists['message']) {
                throw new Exception('');
            }

            $tmp = (Object)array(
                'id' => $id,
            );
            $formDescription = $this->FormDescriptionService->searchByProjectID($tmp);
            if ($formDescription['status'] == 'error') {
                throw new Exception('');
            }

            $guestType = $this->GuestTypeService->searchByProjectID($tmp);
            if ($guestType['status'] == 'error') {
                throw new Exception('無法搜尋');
            }

            return view('form')
                    ->with([
                        'project_ID' => $id,
                        'description' => $formDescription['message']['description'],
                        'guestType' => $guestType['message'],
                    ]);
        } catch (Exception $e) {
            return view('error')
                    ->with([
                        'message' => '查無此表單或發生錯誤'
                    ]);
        }
    }

    public function form_success ($id)
    {
        try {
            $exists = $this->ProjectService->existsByID($id);
            if (!$exists['message']) {
                throw new Exception('');
            }

            return view('formSuccess')
                    ->with([
                        'project_ID' => $id,
                    ]);
        } catch (Exception $e) {
            return view('error')
                    ->with([
                        'message' => '查無此表單或發生錯誤'
                    ]);
        }
    }

    public function pay ($id)
    {
        try {
            $exists = $this->ProjectService->existsByID($id);
            if (!$exists['message']) {
                throw new Exception('');
            }

            return view('pay')
                    ->with([
                        'project_ID' => $id
                    ]);
        } catch (Exception $e) {
            return view('error')
                    ->with([
                        'message' => '查無此表單或發生錯誤'
                    ]);
        }
    }
}