<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Illuminate\Http\Request;
use App\Services\AccountService;

class AccountAPIMiddleware
{
    /** @var Object $AccountService */
    protected $AccountService;

    /**
     * 建構子
     * 
     * @param App\Services\AccountService $AccountService 使用者服務
     * @property App\Services\AccountService $AccountService 使用者服務
     * 
     * @return Void
     */
    public function __construct(AccountService $AccountService)
    {
        $this->AccountService = $AccountService;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        try {
            $verification = $this->AccountService->sessionSignin();
            if ($verification['status'] == 'error') {
                throw new Exception('驗證時發生問題');
            }

            if (!$verification['message']) {
                throw new Exception('無權使用');
            }

            return $next($request);
        } catch (Exception $e) {
            $response = array(
                'status' => 'success',
                'message' => $e->getMessage(),
            );
        }
    }
}
